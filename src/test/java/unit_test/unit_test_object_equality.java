/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package unit_test;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import unit_test.object_equality.Person;
import unit_test.object_equality.SocialInsuranceNumber;

/**
 *
 * @author fred
 */
public class unit_test_object_equality {

    Person p1;
    Person p2;
    Person p3;

    public unit_test_object_equality() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        p1 = new Person("Joe", "Seeber", new SocialInsuranceNumber(123));
        p2 = new Person("Joe", "Seeber", new SocialInsuranceNumber(123));
        p3 = p1;
    }

    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void test1() {
        // 
        //p1=p2;// force error
        assertTrue("p1 should not equal p2", p1!=p2);
    }
    @Test
    public void test2() {
        // 
//        p1=p2;// force error
        assertTrue("p1 should equal p3", p1==p3);
    }
    @Test
    public void test3() {
        // 
//        p1=p2;// force error
        assertTrue("p2 should equal p3", p3==p2);
    }
    @Test
    public void test4() {
        // 
//        p1=p2;// force error
        assertTrue("p2 should equal p3", p3.equals(p2));
    }
    @Test
    public void test5() {
        // 
//        p1=p2;// force error
        assertTrue("p3 should equal p1", p3==p1);
    }
    @Test
    public void test6() {
        // 
//        p1=p2;// force error
        assertTrue("p3 should equal p1", p3.equals(p1));
    }
    
    
    
}
