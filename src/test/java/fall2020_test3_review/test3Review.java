/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fall2020_test3_review;

import ___fall2020.test3_review.*;
import lab4.q2.Car;

import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author students
 */
public class test3Review {
    Camera rebel;
    Camera rebel2;
    Camera fuji;
    Camera pro;
    Car mustang;
    App app;
    
    public test3Review() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        rebel=new Camera("Canon", "Rebel", 12345l);
        rebel2=new Camera("Canon", "Rebel", 12345l);
        fuji=new Camera("Fuji", "Finepix S520", 34567l);
        pro=rebel;// pro and rebel are references.
        mustang=new Car();
        app=new App();
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    @Test
    public void test1() {
        assertTrue(rebel.equals(rebel2));
        
    }
    @Test
    public void test2() {
        assertTrue(pro == rebel);
    }
    @Test
    public void test3() {
        assertFalse(rebel.equals(mustang));
    }
    
    @Test
    public void test4() {
        Camera camera=new Camera();
        app.add(camera);
        assertNotNull(app.findCamera(camera));
    }
    
}
