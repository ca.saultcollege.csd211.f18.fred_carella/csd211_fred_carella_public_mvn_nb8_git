/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test3_review;

import __fall2019.lecture4.lab4b.Car;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author fcarella
 */
public class CarTest {
    private Car car1=new Car("Ford", "Mustang", 2019, null, "1234");
    private Car car2=new Car("Ford", "Mustang", 2019, null, "12345");
    private Car car3=new Car("Ford", "Mustang", 2019, null, "1234");
    private Car car4;
    
    public CarTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
     @Test
     public void testCar() {
         assertFalse("message 1", car1.equals(car2));
         car4=car1;
         assertTrue("message 1", car1.equals(car4));
     }
}
