/*
 * Demonstrates writing a unit test.
 * Here we test lab4.q3.App which contains two methods 
 * useful for testing
 * we create a car and then add it with App.addCarTest(Car)
 * and then confirm it was added with App.getCarTest(int index)
 *
 *
 */
package lab4b;

import lab4.q3.App;
import lab4.q2.Car;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author fcarella
 */
public class lab4b {
    
    public lab4b() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    @Test
    public void addCar() {
        App app=new App();
        Car car=new Car();
        car.setMake("Ford");
        car.setModel("Mustang");
        app.addCarTest(car);
        Car newCar=app.getCarTest(0);
        assertEquals(car, newCar);
    }
}
