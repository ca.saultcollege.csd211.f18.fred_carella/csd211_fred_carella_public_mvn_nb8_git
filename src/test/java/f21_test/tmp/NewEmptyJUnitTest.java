/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package f21_test.tmp;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import f21_test.Car;

/**
 *
 * @author fcarella
 */
public class NewEmptyJUnitTest {
    
    public NewEmptyJUnitTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
     @Test
     public void test1() {
         Car c1=new Car("Ford");
         Car c2=new Car("Ford");
         Car c3=c1;
         Car c4=new Car("Chevy");
         assertEquals(c1, c1);
     }
     @Test
     public void test2() {
         Car c1=new Car("Ford");
         Car c2=new Car("Ford");
         Car c3=c1;
         Car c4=new Car("Chevy");
         assertEquals(c3, c1);
     }
     @Test
     public void test3() {
         Car c1=new Car("Ford");
         Car c2=new Car("Ford");
         Car c3=c1;
         Car c4=new Car("Chevy");
         assertEquals(c3, c2);
     }
     @Test
     public void test4() {
         Car c1=new Car("Ford");
         Car c2=new Car("Ford");
         Car c3=c1;
         Car c4=new Car("Chevy");
         assertNotEquals(c4, c1);
     }
}
