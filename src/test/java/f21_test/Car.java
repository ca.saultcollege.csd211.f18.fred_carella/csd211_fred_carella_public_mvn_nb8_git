/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package f21_test;

/**
 *
 * @author fcarella
 */
public class Car {
    private String make;

    public Car() {
    }

    public Car(String make) {
        this.make = make;
    }

    /**
     * @return the make
     */
    public String getMake() {
        return make;
    }

    /**
     * @param make the make to set
     */
    public void setMake(String make) {
        this.make = make;
    }

    @Override
    public String toString() {
        return make;
    }

    @Override
    public boolean equals(Object obj) {
        if(!(obj instanceof Car))
            return false;
        Car objCar=(Car)obj;
        if(this==objCar)
            return true;
        if(make.equals(objCar.make))
            return true;
        else
            return false;
    }
    
}
