/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ___fall2020;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import ___fall2020.lecture4b.unit_tests.*;

/**
 *
 * @author students
 */
public class CarTest {

    static Car car;
    static Car newCar;
    static App app;

    public CarTest() {
    }

    // run once
    @BeforeClass
    public static void setUpClass() {
        car = new Car("Ford", "Mustang", 2019, 1234l);
        newCar = new Car("Ford", "Mustang", 2019, 1234l);
        app = new App();
    }

    @AfterClass
    public static void tearDownClass() {
    }

    // run before/after each test
    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    @Test
    public void test_1() {
        // car an newCar have the same vin so this test should pass
        assertEquals("Testing cars", car, newCar);
    }

    @Test
    public void test_2() {
        // car and anotherCar do NOT have the same vin so this test should pass
        Car anotherCar = new Car("Ford", "Mustang", 2019, 1234333l);
        assertNotEquals("Testing cars", car, anotherCar);
    }

    @Test
    public void test_3() {
        app.addCar(new Car("Ford", "Mustang", 2019, 1234333l));
        Car foundCar = app.findCar(new Car("Ford", "Mustang", 2019, 1234333l));
        // if car is found, foundCar is not null
        // this test should pass
        assertNotNull(foundCar);

    }
    @Test
    public void test_4() {
        app.addCar(new Car("Ford", "Mustang", 2019, 1234333l));
        Car foundCar = app.findCar(new Car("Ford", "Mustang", 2019, 11111l));
        // since the vins are different foundCar will be null and
        // this test should fail
        assertNotNull(foundCar);

    }
    @Test
    public void test_5() {
        app.addCar(new Car("Ford", "Mustang", 2019, 1234333l));
        Car foundCar = app.findCar(new Car("Ford", "Mustang", 2019, 11111l));
        // since the vins are different foundCar will be null and
        // this test should pass
        assertNull(foundCar);

    }
}
