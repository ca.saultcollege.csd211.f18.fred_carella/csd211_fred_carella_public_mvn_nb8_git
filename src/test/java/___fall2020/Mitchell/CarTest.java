/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ___fall2020.Mitchell;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import ___fall2020.lecture4b.Mitchell.*;

/**
 *
 * @author mitch
 */
public class CarTest {
    static Car car1;
    static Car car2;
    static Car car3;
    static Car car4;
    static App app;
    
    public CarTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        car1 = new Car("Ford", "Mustang", 2019, 1234l);
        car2 = new Car("Ford", "Mustang", 2019, 12345l);
        car3 = new Car("Ford", "Mustang", 2019, 1234l);
        car4 = car1;
        app = new App();
        app.addCar(car1);
        app.addCar(car2);
        app.addCar(car3);
//        app.addCar(car4);
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    @Test
    public void test1() {
        assertTrue("car1 == car4", car1 == car4);
    }
    @Test
    public void test2() {
        assertTrue("car1 != car2", car1 != car2);
    }
    @Test
    public void test3() {
        assertFalse("car1 == car3", car1 == car3);
    }
    @Test
    public void test3b() {
        assertTrue("car1.equals(car3)", car1.equals(car3));
    }
    @Test
    public void test4() {
        Car foundCar = app.findCar(car1);
        assertNotNull(foundCar);
    }
    @Test
    public void test5() {
        Car foundCar = app.findCar(car2);
        assertNotNull(foundCar);
    }
    @Test
    public void test6() {
        Car foundCar = app.findCar(car3);
        assertNotNull(foundCar);
    }
    @Test
    public void test7() {
        Car foundCar = app.findCar(car4);
        assertNotNull(foundCar);
    }
    @Test
    public void test8() {
        app.deleteCar(car3);
        Car foundCar = app.findCar(car3);
        assertNull(foundCar);
    }
    @Test
    public void test9() {
        app.deleteCar(car2);
        Car foundCar = app.findCar(car2);
        assertNull(foundCar);
    }
}
