/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fall2019;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import __fall2019.lecture4b.App;

/**
 *
 * @author fcarella
 */
public class lab4q2 {
    
    public lab4q2() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
     @Test
     public void test1() {
         App app=new App();
         assertTrue("Ok, it should be true", app.run(79927398713L));
//         assertFalse("Ok, it should be true", app.run(79927398710L));
     }
     @Test
     public void test2() {
         App app=new App();
         assertFalse("Not Ok, it should be true", app.run(79927398710L));
     }
     public void test3() {
         App app=new App();
         assertFalse("Ok, it should be true", app.run(79927398711L));
     }
}
