/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package _____fall2021.test34Erika;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author fcarella
 */
public class Test34Erika {
    
    public Test34Erika() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
     @Test
     public void testIfEqual() {
         Course c1=new Course("Intro 2 Java", "csd211", 10);
         Course c2=new Course("Intro 2 Java", "csd211", 10);
         
         assertEquals(c1, c2);
     }
     @Test
     public void testIfNotEqual() {
         Course c1=new Course("Intro 2 Java", "csd211", 10);
         Course c2=new Course("Intro 2 Java", "csd2111", 10);
         
         assertNotEquals(c1, c2);
     
     }
}
