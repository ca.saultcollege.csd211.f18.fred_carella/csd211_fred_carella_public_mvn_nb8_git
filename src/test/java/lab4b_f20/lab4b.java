/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab4b_f20;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import ___fall2020.lecture4b.unit_tests.*;

/**
 *
 * @author students
 */
public class lab4b {
    
    public lab4b() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
     @Test
     public void test_1() {
     
         Car c1=new Car("ford", "mustang",2019, 1234l);
         Car c2=new Car("ford", "mustang",2019, 1234l);
         // should pass
         assertEquals(c1, c2);
     }
     @Test
     public void test_2() {
     
         Car c1=new Car("ford", "mustang",2019, 1234l);
         Car c2=new Car("ford", "mustang",2019, 12345l);
         // should pass
         assertNotEquals(c1, c2);
     }
}
