/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package __fall2019.lecture2.ex6_gui;

import java.util.Scanner;

/**
 *
 * @author fcarella
 */
public class Main {
    public static void main(String[] args) {
        System.out.println("GUI Example");
        GUI gui=new GUI();
        Scanner scanner=new Scanner(System.in);
        System.out.println("Enter radius:");
        int radius=scanner.nextInt();
        gui.setRadius(radius);
        gui.setVisible(true);
    }
}
