/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package __fall2019.lecture2.ex5;

import java.util.Scanner;

/**
 *
 * @author fcarella
 */
public class App {
    // fields
    private static int count;// class variable
    
    public static int getCount(){
        return count;
    }
    public App(){ // this is a constructor it gets called when you create an object
        count++;
    }
    public void run(){
        System.out.println("Application running...");
        // input (hard coded)
        int radius=30;
        double PI=3.14;
        double area;
        
        Scanner input=new Scanner(System.in);
        System.out.println("Enter the radius of the circle:");
        try{
            radius=input.nextInt();
        }catch(Exception e){
            System.out.println("You probably entered an incorrect data type, please try again...");
        }
        // process
        area=Math.PI*radius*radius;
        
        // output
        System.out.println("Area of a circle with radius "+radius+" is "+area);
    }
    public int add(int a, int b){
        int x=a+b; //x, a and b are local to the add method
        return x;
//        return a+b;
    }
    
}
