/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package __fall2019.lecture2.ex2;


/**
 *
 * @author fcarella
 */
public class Main {
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        App app=new App();
        app.setInstanceVariable(10);
//        app.setCount(1);
        app.run();
        
        App app2=new App();
        app2.setInstanceVariable(20);
//        app2.setCount(2);
        app2.run();
        
//        app.setCount(1);
        app.run();
//        new App().run();

        new App();
        System.out.println("App.count="+App.getCount());
    }
    
}
