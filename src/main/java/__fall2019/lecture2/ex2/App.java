/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package __fall2019.lecture2.ex2;

/**
 *
 * @author fcarella
 */
public class App {
    private int instanceVariable; // every instance (object) will have its own copy
    private static int count; // this is a class variable.  Every instance has only one copy

    public App() {// the constructor
        count++;
    }
    
    public void run(){
        int x=10;
        int y=20;
        int z=x+y; // local to the run method
        System.out.println("z="+z);
        print();
    }
    
    public void print(){
        System.out.println("App is running...");
        System.out.println("instanceVariable="+instanceVariable);
        System.out.println("count="+count);
    
    }

    /**
     * @return the instanceVariable
     */
    public int getInstanceVariable() {
        return instanceVariable;
    }

    /**
     * @param instanceVariable the instanceVariable to set
     */
    public void setInstanceVariable(int instanceVariable) {
        this.instanceVariable = instanceVariable;
    }

    /**
     * @return the count
     */
    public static int getCount() {
        return count;
    }

    /**
     * @param aCount the count to set
     */
    public static void setCount(int aCount) {
        count = aCount;
    }
    
}
