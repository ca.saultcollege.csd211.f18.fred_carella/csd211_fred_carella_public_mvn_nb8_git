/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package __fall2019.lecture2.ex3;

/**
 *
 * @author fcarella
 */
public class App {
    // fields
    private int answer; // instance variable
    private static int count;// class variable
    
    public static int getCount(){
        return count;
    }
    public App(){ // this is a constructor it gets called when you create an object
        count++;
    }
    public void run(){
        System.out.println("Application running...");
        answer=add(2,3);
        System.out.println("2+3="+answer);
        
        {// create a block
            int a=10;
            int b=20;
            System.out.println("a*b="+a*b);
        }
        {
            int a=30;
            int b=40;
            System.out.println("a*b="+a*b);
        }
        
        
        
        
    }
    public int add(int a, int b){
        int x=a+b; //x, a and b are local to the add method
        return x;
//        return a+b;
    }
    
}
