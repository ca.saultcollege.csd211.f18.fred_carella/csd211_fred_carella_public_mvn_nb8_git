/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package __fall2019.lecture2.ex1;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author fcarella
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        int num; // java variables are statically type.
                    // you must declare a variable before you can use it.
        Integer number; // reference to an Object of type Integer
        
        number=new Integer(10);
                    
        num=10; // initialize the variable num
        
        double y; // declare a variable of type double (real number)
        float z;  // declare a variable of type float (real  number)
        
        y=10.0d;
        z=10.0f;
        num=(int)10.2;    
//        num=10L;

        

    }
}
