/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package __fall2019.lecture4.ex5;

import java.util.Objects;

/**
 *
 * @author fcarella
 */
public class Person {

    // create attributes
    private int age; // in years
    private int height; // in cm
    private int weight; // in kg
    private String firstName;
    private String lastName;
    private Gender gender; // M/F/O
    private String sin;

    Person() {
        setFirstName("No firstname set");
        setLastName("No lastname set");
        setGender(Gender.O);
    }

    public Person(int age, int height, int weight, String firstName, String lastName, Gender gender, String sin) {
        this.age = age;
        this.height = height;
        this.weight = weight;
        this.firstName = firstName;
        this.lastName = lastName;
        this.gender = gender;
        this.sin=sin;
    }

    Person(String firstname, String lastname, Gender gender, String sin) {
        this(firstname, lastname, sin);
        setGender(gender);
    }

    public enum Gender {
        M, F, O
    }

    public Person(String firstName, String lastName, String sin) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.sin=sin;
    }

    public int getAge() {
        return age;
    }

    public int getHeight() {
        return height;
    }

    public int getWeight() {
        return weight;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    @Override
    public boolean equals(Object o) {
        Person p=(Person)o;// cast o to a Person object
        if(o==this)
            return true;
        if(this.sin.equals(p.getSin())){
            return true;
        }else
            return false;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 23 * hash + Objects.hashCode(this.firstName);
        hash = 23 * hash + Objects.hashCode(this.lastName);
        return hash;
    }

    /**
     * @return the firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * @param firstName the firstName to set
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * @return the lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * @param lastName the lastName to set
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * @return the gender
     */
    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    @Override
    public String toString() {
        return firstName + " " + lastName + ", " + getGender();
//        return super.toString(); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * @return the sin
     */
    public String getSin() {
        return sin;
    }

    /**
     * @param sin the sin to set
     */
    public void setSin(String sin) {
        this.sin = sin;
    }

}
