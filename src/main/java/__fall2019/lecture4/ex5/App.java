/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package __fall2019.lecture4.ex5;

/**
 *
 * @author fcarella
 */
public class App {

    public App() {
    }

    public void run() {
        
        Person p1=new Person("Dharti", "Patel", "12345");
        Person p2=new Person("Dharti", "Patel", "12345");
        Person p3=p1;
        System.out.println(p1);
        System.out.println(p2);
        
//        p1=p2;
        
        if(p1.equals(p2))
            System.out.println("p1==p2");
        else
            System.out.println("p1!=p2");
      if(p1==p2)// wrong answer.  Must use .equals
            System.out.println("p1==p3");
        else
            System.out.println("p1!=p3");
        
    }

}
