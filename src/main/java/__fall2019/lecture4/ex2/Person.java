/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package __fall2019.lecture4.ex2;


/**
 *
 * @author fcarella
 */
public class Person {
    // create attributes
    private int age; // in years
    private int height; // in cm
    private int weight; // in kg
    private String firstName;
    private String lastName;
    private Gender gender; // M/F/O
    public enum Gender {
        M, F, O
}
    
    public int getAge(){
        return age;
    }
    public int getHeight(){
        return height;
    }
    public int getWeight(){
        return weight;
    }
    public void setAge(int age){
        this.age=age;
    }
    public void setHeight(int height){
        this.height=height;
    }

    /**
     * @return the firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * @param firstName the firstName to set
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * @return the lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * @param lastName the lastName to set
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * @return the gender
     */
    public Gender getGender() {
        return gender;
    }

    public void setGender2(Gender gender){
        this.gender=gender;
    }
    
}
