/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package __fall2019.lecture4.ex2;

/**
 *
 * @author fcarella
 */
public class App {

    public App() {
    }

    public void run() {
        Person dharti = new Person();
        dharti.setFirstName("Dharti");
        dharti.setLastName("Patel");
        dharti.setAge(20);
        
        dharti.setGender2(Person.Gender.F);
        
        System.out.println("Person 1: "
                + dharti.getFirstName() + ", "
                + dharti.getLastName() + ", "
                + dharti.getGender() + ", "
                + dharti.getAge()
        );
    }

}
