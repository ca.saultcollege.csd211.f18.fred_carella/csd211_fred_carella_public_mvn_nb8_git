/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package __fall2019.lecture4.lab4.q1;

/**
 *
 * @author fcarella
 */
public class App {

    public App() {
    }

    public void run() {
        Person p1 = new Person("Joe", "Student", 23, Person.Gender.M);
        Person p2 = new Person("George", "Student", 24, 188.2, 60.0, Person.Gender.M);

// Mary Teacher is female, 43 years old, is 170.00 cm tall and weighs 48.2 kg.
        Person p3 = new Person("Mary", "Teacher", 43, 170, 48.2, Person.Gender.F);
        System.out.println(p1);
        System.out.println(p2);
        System.out.println(p3);

    }

}
