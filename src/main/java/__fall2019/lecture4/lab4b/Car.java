/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package __fall2019.lecture4.lab4b;

import __fall2019.lecture4.lab4.q2.*;

/**
 *
 * @author fcarella
 */
public class Car {

    private String make;
    private String model;
    private int year;
    private Person owner;
    private String vin;

    public Car() {
    }

    public Car(String Make, String Model, int Year, Person Owner, String VIN) {
        this.make = Make;
        this.model = Model;
        this.year = Year;
        this.owner = Owner;
        this.vin = VIN;
    }

    @Override
    public String toString() {
        return "Make=" + getMake() + " Model=" + getModel(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof Car)) {
            return false;
        }
        Car car = (Car) o;

        if (car.vin.equals(this.vin)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @return the make
     */
    public String getMake() {
        return make;
    }

    /**
     * @param make the make to set
     */
    public void setMake(String make) {
        this.make = make;
    }

    /**
     * @return the model
     */
    public String getModel() {
        return model;
    }

    /**
     * @param model the model to set
     */
    public void setModel(String model) {
        this.model = model;
    }

    /**
     * @return the year
     */
    public int getYear() {
        return year;
    }

    /**
     * @param year the year to set
     */
    public void setYear(int year) {
        this.year = year;
    }

    /**
     * @return the owner
     */
    public Person getOwner() {
        return owner;
    }

    /**
     * @param owner the owner to set
     */
    public void setOwner(Person owner) {
        this.owner = owner;
    }

    /**
     * @return the vin
     */
    public String getVin() {
        return vin;
    }

    /**
     * @param vin the vin to set
     */
    public void setVin(String vin) {
        this.vin = vin;
    }

}
