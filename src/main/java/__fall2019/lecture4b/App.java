/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package __fall2019.lecture4b;

/**
 *
 * @author fcarella
 */
public class App {

    public App() {
    }

    public void run() {
        long[] nums2check={79927398710L, 79927398711L, 79927398712L, 79927398713L, 79927398714L, 79927398715L, 79927398716L, 79927398717L, 79927398718L, 79927398719L};
        for(long num2check:nums2check){
            if(isValid(num2check)){
                System.out.println(num2check+" is valid");
            }else
                System.out.println(num2check+" is NOT valid");
        }
    }
    public boolean run(long number2check) {
        return isValid(number2check);
    }

    private boolean isValid(long accountNumber2bChecked) {
// use this test data, where accountNumber2bChecked are
        // 79927398710, 79927398711, 79927398712, 79927398713, 79927398714, 79927398715, 79927398716, 79927398717, 79927398718, 79927398719
        // the only valid number should be 79927398713

        // this should be a valid number
//        long accountNumber2bChecked = 79927398713L;
        long accountNumber2bCheckedBu = accountNumber2bChecked;
        // separate the check digit
        long checkDigit = accountNumber2bChecked % 10;
        accountNumber2bChecked /= 10;// truncate last digit
        long tmp = accountNumber2bChecked;
        // INPUTS - separate each digit
        long lsb0 = (long) tmp % 10;
        tmp = tmp / 10;
        long lsb1 = (long) tmp % 10;
        tmp = tmp / 10;
        long lsb2 = (long) tmp % 10;
        tmp = tmp / 10;
        long lsb3 = (long) tmp % 10;
        tmp = tmp / 10;
        long lsb4 = (long) tmp % 10;
        tmp = tmp / 10;
        long lsb5 = (long) tmp % 10;
        tmp = tmp / 10;
        long lsb6 = (long) tmp % 10;
        tmp = tmp / 10;
        long lsb7 = (long) tmp % 10;
        tmp = tmp / 10;
        long lsb8 = (long) tmp % 10;
        tmp = tmp / 10;
        long lsb9 = (long) tmp % 10;

        // PROCESS - start processing the data
        // double every second digit
        long lsb0x2 = lsb0 * 2;
        long lsb2x2 = lsb2 * 2;
        long lsb4x2 = lsb4 * 2;
        long lsb6x2 = lsb6 * 2;
        long lsb8x2 = lsb8 * 2;

        // sum the digits of the products
        // first separate each digit
        // 
        long lsb0x2lsb0 = lsb0x2 % 10;
        lsb0x2 = lsb0x2 / 10;
        long sumlsb0 = lsb0x2 + lsb0x2lsb0;
        //
        long lsb2x2lsb0 = lsb2x2 % 10;
        lsb2x2 = lsb2x2 / 10;
        long sumlsb2 = lsb2x2 + lsb2x2lsb0;
        //
        long lsb4x2lsb0 = lsb4x2 % 10;
        lsb4x2 = lsb4x2 / 10;
        long sumlsb4 = lsb4x2 + lsb4x2lsb0;
        //
        long lsb6x2lsb0 = lsb6x2 % 10;
        lsb6x2 = lsb6x2 / 10;
        long sumlsb6 = lsb6x2 + lsb6x2lsb0;
        //
        long lsb8x2lsb0 = lsb8x2 % 10;
        lsb8x2 = lsb8x2 / 10;
        long sumlsb8 = lsb8x2 + lsb8x2lsb0;
        //
        long sumOfDigits = sumlsb0 + lsb1 + sumlsb2 + lsb3 + sumlsb4 + lsb5 + sumlsb6 + lsb7 + sumlsb8 + lsb9;

//        System.out.println(sumOfDigits);

        // OUTPUT
        sumOfDigits += checkDigit;
        if (sumOfDigits % 10 == 0) {
//            System.out.println("The account " + accountNumber2bCheckedBu + " is a valid account number.");
            return true;
        } else {
//            System.out.println("The account " + accountNumber2bCheckedBu + " is NOT a valid account number.");
            return false;
        }

    }

}
