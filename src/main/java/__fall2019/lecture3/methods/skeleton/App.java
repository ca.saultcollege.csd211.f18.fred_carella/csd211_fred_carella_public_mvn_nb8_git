/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package __fall2019.lecture3.methods.skeleton;

import java.util.Scanner;

/**
 *
 * @author fcarella
 */
public class App {
    private Scanner input=new Scanner(System.in);

    public App() {
    }
    public void run(){
        boolean done=false;
        int choice=0;
        while(!done){
            System.out.println("1. Add");
            System.out.println("2. Subtract");
            System.out.println("99. Exit");
            choice=input.nextInt();
            switch(choice){
                case 1: add();
                    break;
                case 2: subtract();
                    break;
                case 99: System.out.println("Good bye");
                    System.exit(0);
                    break;
                default: System.out.println("Sorry try again");
                    break;
            }
        }
    }
    private void add(){
        System.out.println("Add");
    }
    private void subtract(){
        System.out.println("Subtract");
    }}
