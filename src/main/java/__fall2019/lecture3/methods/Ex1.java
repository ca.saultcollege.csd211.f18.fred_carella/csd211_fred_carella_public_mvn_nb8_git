/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package __fall2019.lecture3.methods;

/**
 *
 * @author fcarella
 */
public class Ex1 {
    private static String firstname;
    
    public static void main(String[] args) {
        int x=10;
        int y=20;
        int result;
        result = add(x,y);
    }
   // visibility modifier, <static>, return type,
    // name
    private static String getFirstname(){
        return firstname;
    }
    private static void setFirstname(String firstname){
//        this.firstname=firstname;
    }
    private static int add(int x, int y){
        int z=x+y;
        return z;
    }
    
    private static double add(double x, double y){
        double z=x+y;
        return z;
    }
    
}
