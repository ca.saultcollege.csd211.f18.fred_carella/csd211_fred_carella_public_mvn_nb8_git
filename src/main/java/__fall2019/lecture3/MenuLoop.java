/*
 * MenuLoop.java
 * 
 * Author :         Fred Carella
 * Date:            2015
 * Description :    Demonstrate a console based menu system using
                    while loops,
                    switch case

 */
package __fall2019.lecture3;

import java.util.Scanner;

/**
 *
 * @author fcarella
 */
public class MenuLoop {
//    private final Scanner input =new Scanner(System.in);
    
    public static void main(String[] args) {
        boolean quit=false;
        String mainMenu="" +
                "1. Option 1\n" +
                "2. Option 2\n" +
                "3. Option 3\n" +
                "4. Option 4\n" +
                "5. Quit\n\n" +
                "";
        Scanner input=new Scanner(System.in);
        while(!quit){
            System.out.println(mainMenu);
            int choice = input.nextInt();
            switch(choice){
                case 1:
                    doOption1();
                    break;
                case 2:
                    doOption2();
                    break;
                case 3:
                    doOption3();
                    break;
                case 4:
                    doOption4();
                    break;
                case 5:
                    quit=true;
                    break;
                default:
                    System.out.println("Invalid entry try again");


            }
        }
        System.exit(0);
    }
    private static void doOption1(){
        System.out.println("Doing option 1 right now");

    }

    private static void doOption2() {
        System.out.println("Doing option 2 right now");
    }

    private static void doOption3() {
        System.out.println("Doing option 3 right now");
    }

    private static void doOption4() {
        System.out.println("Doing option 4 right now");

    }


}
