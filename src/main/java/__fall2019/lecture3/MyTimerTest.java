/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package __fall2019.lecture3;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Scanner;

/**
 *
 * @author fcarella
 */
public class MyTimerTest {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String pattern = "MMM-dd-yyyy hh:mmaa";
        SimpleDateFormat format = new SimpleDateFormat(pattern);
        Calendar currDate = new GregorianCalendar();

        Calendar sprinkler1Start = new GregorianCalendar();
        Calendar sprinkler1End = new GregorianCalendar();
        Calendar sprinkler2Start = new GregorianCalendar();
        Calendar sprinkler2End = new GregorianCalendar();
        sprinkler1Start.set(2012, 0, 1, 8, 0);// yr, mo, day, hr, min 8 am
        sprinkler1End.set(2012, 0, 1, 10, 0);// yr, mo, day, hr, min 10 am

        long s1 = sprinkler1Start.getTimeInMillis();

        System.out.println(format.format(sprinkler1Start.getTime()));

        int x1 = sprinkler1Start.get(currDate.HOUR_OF_DAY);// 0=midnight

        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter date and time in the format MMM-dd-yyyy hh:mmaa");
        System.out.println("For example, it is now " + format.format(sprinkler1End.getTime()));
        Date date = null;
        while (date == null) {
            String line = scanner.nextLine();
            try {
                date = format.parse(line);
            } catch (ParseException e) {
                System.out.println("Sorry, that's not valid. Please try again.");
            }
        }

    }
}
