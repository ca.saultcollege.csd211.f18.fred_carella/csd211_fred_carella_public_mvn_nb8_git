/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package __fall2019.lecture3.ex2;

import java.util.Scanner;

/**
 *
 * @author fcarella
 */
public class App {

    public void run() {
        String menuString = "Simple Calculator" + "\n"
                + "1. Add 2 numbers" + "\n"
                + "2. Subtract 2 numbers" + "\n"
                + "3. Multiply 2 numbers" + "\n"
                + "4. Divide 2 numbers" + "\n"
                + "Please enter the operation to perform (1, 2, 3 or 4) : " + "\n";
        System.out.println("" + menuString);

        Scanner in = new Scanner(System.in);
        int choice = in.nextInt();

        if (choice == 1) {
            System.out.println("You chose option 1");
        } else if (choice == 2) {
            System.out.println("You chose option 2");
        } else if (choice == 3) {
            System.out.println("You chose option 3");
        } else if (choice == 4) {
            System.out.println("You chose option 4");
        } else {
            System.out.println("You entered a bad choice");
        }

    }
}
