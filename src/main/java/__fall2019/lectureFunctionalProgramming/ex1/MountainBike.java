/*
 * See https://docs.oracle.com/javase/tutorial/java/javaOO/classes.html
 */
package __fall2019.lectureFunctionalProgramming.ex1;

public class MountainBike extends Bicycle {
        
    // the MountainBike subclass has
    // one field
    private int seatHeight;

    // the MountainBike subclass has
    // one constructor
    public MountainBike(int startHeight, int startCadence,
                        int startSpeed, int startGear) {
        super(startCadence, startSpeed, startGear);
        seatHeight = startHeight;
    }   
        
    // the MountainBike subclass has
    // one method
    public void setHeight(int newValue) {
        seatHeight = newValue;
    }   

}