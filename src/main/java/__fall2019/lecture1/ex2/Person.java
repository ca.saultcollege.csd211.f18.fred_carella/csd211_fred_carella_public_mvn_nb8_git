/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package __fall2019.lecture1.ex2;

/**
 *
 * @author fcarella
 */
public class Person {
    // state fields (variables )
    // make these private NOT public
    private String firstname;
    private String lastname;
    
    public void setFirstname(String name){
        firstname=name;
    }
    public String getFirstname(){
        return firstname;
    }

    /**
     * @return the lastname
     */
    public String getLastname() {
        return lastname;
    }

    /**
     * @param lastname the lastname to set
     */
    public void setLastname(String lastname) {
        this.lastname = lastname;
    }
}
