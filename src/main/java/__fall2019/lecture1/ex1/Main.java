/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package __fall2019.lecture1.ex1;

/**
 *
 * @author fcarella
 */
public class Main {
    public static void main(String[] args) {
        System.out.println("Hello World!");
        Person fred=new Person();// create a new Object called "fred" based on the Person class
        Person george=new Person();
        george.firstName="George";
        Person martha=george;
//        george=null;
//        Person joe=null;
        
        
        System.out.println("fred.firstName="+fred.firstName);
        System.out.println("george.firstName="+george.firstName);
//        System.out.println("joe.firstName="+joe.firstName);
    }
}
