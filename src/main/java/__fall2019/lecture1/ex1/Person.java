/*
 * This is a class or "blueprint" of a Person object
 * (its not THE object, its a decription of the Person object)
 */
package __fall2019.lecture1.ex1;

/**
 *
 * @author fcarella
 */
public class Person {
    public String firstName="Fred";// public attribute called "firstName"
}
