package __fall2019.lecture1.ex1;

public class NameOfTheClass {
    // the name of a class is the same as the name of the file
    public static void main(String[] args){
        System.out.println("args[0]=="+args[0]);
        System.out.println("args[1]=="+args[1]);
        System.out.println("args[2]=="+args[2]);
    }
}
