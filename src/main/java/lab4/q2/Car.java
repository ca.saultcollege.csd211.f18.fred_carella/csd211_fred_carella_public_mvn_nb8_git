/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab4.q2;

import lab4.q1.Person;

/**
 *
 * @author fred
 */
public class Car {
    private String make;
    private String model;
    
    private Person owner;

    public Car() {
    }

    public Car(String make) {
        this.make = make;
    }
    public Car(String make, String model) {
        this.make = make;
        this.model = model;
    }

    public Car(String make, Person owner) {
        this.make = make;
        this.owner = owner;
    }

    @Override
    public String toString() {
        return getMake()+"\nOwner:\n"+getOwner();
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj); //To change body of generated methods, choose Tools | Templates.
    }
    

    /**
     * @return the make
     */
    public String getMake() {
        return make;
    }

    /**
     * @param make the make to set
     */
    public void setMake(String make) {
        this.make = make;
    }

    /**
     * @return the owner
     */
    public Person getOwner() {
        return owner;
    }

    /**
     * @param owner the owner to set
     */
    public void setOwner(Person owner) {
        this.owner = owner;
    }

    /**
     * @return the model
     */
    public String getModel() {
        return model;
    }

    /**
     * @param model the model to set
     */
    public void setModel(String model) {
        this.model = model;
    }
    
}
