/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab4.q1;

/**
 *
 * @author fred
 */
public class Main {
    public static void main(String[] args) {
        Person p=new Person();
        p.setFirstname("Joe");
        p.setLastname("Blo");
//        System.out.println("Hi "+p.getFirstname()+" "+p.getLastname());
        System.out.println(p);
        
        Person joe=new Person("Joe", "Blo");
        System.out.println(joe);
        
        if(p.equals(joe)){
            System.out.println(p+" is the same as "+ joe);
        }
        
        if(p==joe){
            System.out.println(p+" is the same as "+ joe);
        }else{
            System.out.println(p+" is NOT the same as "+ joe);
        }
        
    }
   
}
