/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab4.q3;

import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import lab4.q2.Car;

/**
 *
 * @author fred
 */
public class App {

    private static Car[] cars = new Car[100];
    private static int currentCar = 0;

    public void run() {
        boolean quit = false;
        String mainMenu = ""
                + "1. List Cars\n"
                + "2. Add Car\n"
                + "3. Option 3\n"
                + "4. Option 4\n"
                + "5. Quit\n\n"
                + "";
        String option1 = "1. Do option 1 :";
        Scanner input = new Scanner(System.in);
        while (!quit) {
            System.out.println(mainMenu);
            int choice = input.nextInt();
            switch (choice) {
                case 1:
                    listCars();
                    break;
                case 2: {
                    try {
                        addCar();
                        listCars();
                    } catch (Exception ex) {
                        Logger.getLogger(App.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                break;
                case 3:
                    doOption3();
                    break;
                case 4:
                    doOption4();
                    break;
                case 5:
                    quit = true;
                    break;
                default:
                    System.out.println("Invalid entry try again");

            }
        }
        System.exit(0);
    }

    private void listCars() {
        System.out.println("\nList all cars");
        System.out.println("-------------");
        for(Car car:cars){
            if(car==null)
                break;
            System.out.println(car);
        }
    }

    private void addCar() throws Exception {
        try {
            Scanner input = new Scanner(System.in);
            System.out.println("Add Car");
            System.out.println("Enter Make");
            String make = input.next();
            Car car = new Car(make);
            cars[currentCar]=car;
            currentCar++;
//            throw new Exception();
        } catch (Exception e) {
            throw new Exception("Error Adding a car");
        }
    }

    public void addCarTest(Car car){
        cars[currentCar]=car;
        currentCar++;
    }
    
    public Car getCarTest(int index){
//        return cars[index];
        return new Car();
    }
    
    private void doOption3() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void doOption4() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
