/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test2_review;

/**
 *
 * @author fcarella
 */
public class Animal {

    /**
     * @return the count
     */
    public static int getCount() {
        return count;
    }

    /**
     * @param aCount the count to set
     */
    public static void setCount(int aCount) {
        count = aCount;
    }

    /**
     * @return the PI
     */
    public static double getPI() {
        return PI;
    }

    /**
     * @param aPI the PI to set
     */
    public static void setPI(double aPI) {
        PI = aPI;
    }
    // Attributes (Properties)
    
    // instance variables
    // every instance of an object has its own copy
    private String species;
    private int numLegs;
    private double weight;
    
    // class variable
    // only one copy for all instances
    private static int count;
    
    // class constant
    private static double PI=3.14;

    
    // constructor is executed when the object is created
    // A specila method, it has the same name as the class
    // Note the signature
    public Animal() {
        count++;
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize(); //To change body of generated methods, choose Tools | Templates.
        count--;
    }
    
    
    public String getSpecies(){
        return species;
    }
    // x is a variable thats local to setSpecies
    public void setSpecies(String species){
        this.species=species;
    }

    /**
     * @return the numLegs
     */
    public int getNumLegs() {
        return numLegs;
    }

    /**
     * @param numLegs the numLegs to set
     */
    public void setNumLegs(int numLegs) {
        this.numLegs = numLegs;
    }

    /**
     * @return the weight
     */
    public double getWeight() {
        return weight;
    }

    /**
     * @param weight the weight to set
     */
    public void setWeight(double weight) {
        this.weight = weight;
    }
}
