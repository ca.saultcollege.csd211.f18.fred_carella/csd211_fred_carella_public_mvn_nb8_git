/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test2_review;

/**
 *
 * @author fcarella
 */
public class App {
    public void run(){
        Animal cat=new Animal();
        Animal dog=new Animal();
        
        cat.setSpecies("felines");
        dog.setSpecies("canine");
        System.out.println("Cat species="+cat.getSpecies());
        System.out.println("Dog species="+dog.getSpecies());

        System.out.println("There are "+Animal.getCount()+" in my zoo");
        
        // delete cat
        cat=null;
        System.gc();
        for(long l=0;l<1000000000;l++){
            l=l;
        }
        System.out.println("There are "+Animal.getCount()+" in my zoo");
    }
}
