/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package _____fall2021.test34Erika;

import java.util.Scanner;

/**
 *
 * @author fcourseella
 */
public class App {

    private Course[] courses = new Course[100];
    private int currentIndex = 0;
    private Scanner input;
    private String menu = ""
            + "1. Add Course\n"
            + "2. List Courses\n"
            + "3. Edit Course\n"
            + "99. quit";

    public void run() {
        System.out.println("Running App...");

//        Course csd211=new Course();
//        csd211.setCourseName("Intro to Java");
//        csd211.setCourseCode("csd211");
//        
//        Course csd211_2=new Course("Intro to Java", "csd2111", 10);
//        
//        if(csd211.equals(csd211_2))
//            System.out.println("Theyre equal");
//        else
//            System.out.println("Theyre NOT equal");
//        
//        
//        System.out.println("csd211="+csd211);
        final int numCourses = 100;

        boolean done = false;
        while (!done) {
            System.out.println(menu);
            input = new Scanner(System.in);
            System.out.println("Enter your choice:");
            int choice = input.nextInt();
            switch (choice) {
                case 1:
                    addCourse();
                    break;
                case 2:
                    listCourses();
                    break;
                case 3:
                    editCourse();
                    break;
                case 99:
                    done = true;
                default:
                    System.out.println("Wrong choice try again...");
                    break;
            }

        }

    }

    private void addCourse() {
        input = new Scanner(System.in);
        System.out.println("Enter Course Code");
        String cc=getInput("");
        System.out.println("Enter Course Name");
        String cn=getInput("");
        Course course=new Course(cn, cc, 10);
        courses[currentIndex]=course;
        currentIndex++;
    }

    private void listCourses() {
        for(int i=0;i<courses.length;i++){
            if(courses[i]==null)
                break;
            System.out.println( (i+1)+". "+courses[i]);
            
        }
    }


    private void editCourse() {
        listCourses();
        System.out.println("Which course would you like to edit ?:");
        int choice = input.nextInt();
        input = new Scanner(System.in); // reset the scanner
        if ((choice < currentIndex + 1) && choice > 0) {
            Course c = courses[choice - 1];
            System.out.println("Code: " + c.getCourseCode());
            c.setCourseCode(getInput(c.getCourseCode()));
            System.out.println("Name: " + c.getCourseName());
            c.setCourseName(getInput(c.getCourseName()));
        } else {
            System.out.println("Choice out of bounds");
        }
        System.out.println("");
    }

    private String getInput(String s) {
        String ss = input.nextLine();
        if (ss.trim().isEmpty()) {
            return s;
        }
        Scanner in2 = new Scanner(ss);
        return in2.nextLine();
    }

    private int getInput(int i) {
        String s = input.nextLine();
        if (s.trim().isEmpty()) {
            return i;
        }
        Scanner in2 = new Scanner(s);
        return in2.nextInt();
    }

    private double getInput(double i) {
        String s = input.nextLine();
        if (s.trim().isEmpty()) {
            return i;
        }
        Scanner in2 = new Scanner(s);
        return in2.nextDouble();
    }

}
