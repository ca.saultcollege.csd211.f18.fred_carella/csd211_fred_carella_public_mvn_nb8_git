/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package _____fall2021.test34Erika;

/**
 *
 * @author fcarella
 */
public class Course {
    
    // private instance variables
    private String courseName;
    private String courseCode;
    private int numberOfStudents;
    // class variable
    private static int COUNT;

    public Course() {
        this.courseName="empty course name";
        this.courseCode="empty course code";
        this.COUNT++;
    }

    public Course(String courseName, String courseCode, int numberOfStudents) {
        this.courseName = courseName;
        this.courseCode = courseCode;
        this.numberOfStudents = numberOfStudents;
        this.COUNT++;
    }

    @Override
    public String toString(){
        return "["+courseCode+", ["+courseName+"]";
    }
    
    public boolean equals(Object obj){
        Course c;
        if (obj instanceof Course)
            c=(Course)obj;
        else
            return false;
        
        if(this == obj)
            return true;
        
        if(this.getCourseCode().equals(c.getCourseCode()))
            return true;
        
        return false;
    }
    
    /**
     * @return the courseName
     */
    public String getCourseName() {
        return courseName;
    }

    /**
     * @param courseName the courseName to set
     */
    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    /**
     * @return the courseCode
     */
    public String getCourseCode() {
        return courseCode;
    }

    /**
     * @param courseCode the courseCode to set
     */
    public void setCourseCode(String courseCode) {
        this.courseCode = courseCode;
    }

    /**
     * @return the numberOfStudents
     */
    public int getNumberOfStudents() {
        return numberOfStudents;
    }

    /**
     * @param numberOfStudents the numberOfStudents to set
     */
    public void setNumberOfStudents(int numberOfStudents) {
        this.numberOfStudents = numberOfStudents;
    }

    /**
     * @return the COUNT
     */
    public static int getCOUNT() {
        return COUNT;
    }

    /**
     * @param aCOUNT the COUNT to set
     */
    public static void setCOUNT(int aCOUNT) {
        COUNT = aCOUNT;
    }
    
    
    
    
}
