/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lecture3;

/**
 *
 * @author fcarella
 */
public class F15 {

    public static void main(String[] args) {
//        while(true){
//            // endless loop
//            System.out.println("Endless loop");
//        }
        int x = 4;
        while (x == 4) {
            System.out.println("Endless loop");
            ++x;
        }
        x=4;
        while(x-- > 0){// 4 3 2 1 0
            System.out.println("x="+x);
        }
        int y=5;
        x=4;
        while( (x<4) && (y>=5) ){
        
        }
        while(x>=0 && x<10){
        }
        x=5;
        while(x-- >0){
            // do something 5 x
        }
        x=5;
        do{
            // do something 5 x
        }while(x-- > 0);
        
        
    }
}
