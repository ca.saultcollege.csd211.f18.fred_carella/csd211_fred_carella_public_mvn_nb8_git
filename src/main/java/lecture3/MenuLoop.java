/*
 * MenuLoop.java
 * 
 * Author :         Fred Carella
 * Date:            2015
 * Description :    Demonstrate a console based menu system using
                    while loops,
                    switch case

 */
package lecture3;

import java.util.Scanner;

/**
 *
 * @author fcarella
 */
public class MenuLoop {
    private final Scanner input =new Scanner(System.in);
    
    public static void main(String[] args) {
        boolean done=false;
        
        while(!done){
            
        }
    }
}
