/*
 */
package fall2016_lecture4.ex1;

/**
 * @author fcarella
 */
public class Person {

    private String firstName;
    private String lastName;
    private int age;
    private static int count;
    
    public Person(){
        this.firstName="<first name not set>";
        this.lastName="<last name not set>";
    }
    public Person(String firstName, String lastName){
        setFirstName(firstName);
        setLastName(lastName);
    }
    // create accessors
    public String getFirstName(){
        return firstName;
    }
    public String getLastName(){
        return lastName;
        
    }
    // create mutators
    public void setFirstName(String firstName){
        this.firstName=firstName;
    }
    public void setLastName(String lastName){
        this.lastName=lastName;
    }

    @Override
    public String toString() {
        return getFirstName()+" "+getLastName();
    }
    
    

    /**
     * @return the age
     */
    public int getAge() {
        return age;
    }

    /**
     * @param age the age to set
     */
    public void setAge(int age) {
        this.age = age;
    }
    /**
     * @return the count
     */
    public static int getCount() {
        return count;
    }

    /**
     * @param aCount the count to set
     */
    public static void setCount(int aCount) {
        count = aCount;
    }

    @Override
    public boolean equals(Object obj) {
    
            if(! (obj instanceof Person))
                return false;
            if(this==obj)
                return true;
            Person objX=(Person)obj;
            if(
                    this.getFirstName().equals( objX.getFirstName())
                    &&
                    this.getLastName().equals( objX.getLastName())
                    )
                return true;
            else return false;
    }
    
    
}
