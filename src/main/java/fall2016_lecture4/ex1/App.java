/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fall2016_lecture4.ex1;

import java.util.Scanner;

/**
 *
 * @author fcarella
 */
public class App extends Object{
    public void run(){
        Person todd=new Person();
        todd.setFirstName("Todd");
        todd.setLastName("Pidgeon");
        
        Person mike=new Person("Mike", "Blanchard");
        
        
        System.out.println("Firstname ="+todd.getFirstName());
        System.out.println("Lastname ="+todd.getLastName());
        
        System.out.println("Firstname ="+mike.getFirstName());
        System.out.println("Lastname ="+mike.getLastName());
        
        System.out.println("Todd="+todd);
        System.out.println("Todd="+mike);
        
        if(todd==mike){
            System.out.println("Equal");
        }
        
        Person boss=todd;
        
        if(todd.equals(boss))
            System.out.println("Todd is boss");
    }
}
