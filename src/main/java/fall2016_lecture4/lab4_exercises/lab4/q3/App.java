/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fall2016_lecture4.lab4_exercises.lab4.q3;

import java.util.Scanner;

/**
 *
 * @author fcarella
 */
public class App {
    Scanner input=new Scanner(System.in);
    private Car[] cars=new Car[100];// this does NOT create 100 cars
    private int currentCar=0;
    
    public void run(){
        try{
            System.err.println("Car[0]"+cars[0].getMake());
        }catch(Exception e){
            System.out.println("Oops cant do that!");
        }
        
        Car c1=new Car("c1");
        System.out.println("c1="+c1);
        Car c2=new Car("c2");
        System.out.println("c2="+c2);
        cars[0]=c1;
        cars[1]=c1;
        cars[5]=c2;
        System.out.println("cars[0]="+cars[0]);
        System.out.println("cars[1]="+cars[1]);
        System.out.println("cars[5]="+cars[5]);
        c2=null;
        System.out.println("cars[0]="+cars[0]);
        System.out.println("cars[1]="+cars[1]);
        System.out.println("cars[5]="+cars[5]);
        
        System.out.println("c1="+c1);
        
        printCars();
        
        
    }
    private void printCars(){
        for(int i=0; i<=cars.length-1;i++){
            if(cars[i]!=null)
                System.out.println("Cars["+i+"]=="+cars[i]);
        }
    }
}
