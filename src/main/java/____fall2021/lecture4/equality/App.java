/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ____fall2021.lecture4.equality;

/**
 *
 * @author fcarella
 */
public class App {

    private Person person1;
    private Person person2;
    private Person person3;

    public void run() {
        // we intend for person1 and person2 and person3 to represent the same person
        person1 = new Person("George", "Washington", 1234);
        person2 = new Person("George", "Washington", 1234);
        person3 = person1;
        checkEqualityTheWrongWay();
        // we intend for person1, person2 and person3 to be the smae person but we spell the name wrong
        // this should result in them not being equal
        person1.setFirstname("Georg");
        checkEqualityTheWrongWay();
        
        // lets do it the right way
        // reset Georges first name so that they are all equal again
        person1.setFirstname("George");
        checkEqualityTheRightWay();
        person1.setFirstname("Georg");
        checkEqualityTheRightWay();
    }
    private void checkEqualityTheWrongWay() {
        System.out.println("---------------------------");
        if (person1 == person1) {
            System.out.println("Person1 and Person1 are the same person");
        } else {
            System.out.println("Person1 and Person1 ARE NOT the same person");
        }        
        if (person1 == person2) {
            System.out.println("Person1 and Person2 are the same person");
        } else {
            System.out.println("Person1 and Person2 ARE NOT the same person");
        }
        if (person1 == person3) {
            System.out.println("Person1 and Person3 are the same person");
        } else {
            System.out.println("Person1 and Person3 ARE NOT the same person");
        }
        if (person2 == person3) {
            System.out.println("Person2 and Person3 are the same person");
        } else {
            System.out.println("Person2 and Person3 ARE NOT the same person");
        }
        System.out.println("---------------------------");
    }
    private void checkEqualityTheRightWay() {
        System.out.println("---------------------------");
        if (person1.equals(person1)) {
            System.out.println("Person1 and Person1 are the same person");
        } else {
            System.out.println("Person1 and Person1 ARE NOT the same person");
        }        
        if (person1.equals(person2)) {
            System.out.println("Person1 and Person2 are the same person");
        } else {
            System.out.println("Person1 and Person2 ARE NOT the same person");
        }
        if (person1.equals(person3)) {
            System.out.println("Person1 and Person3 are the same person");
        } else {
            System.out.println("Person1 and Person3 ARE NOT the same person");
        }
        if (person2.equals(person3)) {
            System.out.println("Person2 and Person3 are the same person");
        } else {
            System.out.println("Person2 and Person3 ARE NOT the same person");
        }
        System.out.println("---------------------------");
    }
}
