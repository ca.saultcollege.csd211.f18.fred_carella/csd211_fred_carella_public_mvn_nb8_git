/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ____fall2021.lecture4.equality;

import ____fall2021.lecture3.ApplicationSkeleton.ex2.*;

/**
 *
 * @author fcarella
 */
public class Person {
    private String firstname;
    private String lastname;
    private int sin;

    public Person() {
        setFirstname("default firstname");
        setLastname("default lastname");
        setSin(-1);
    }

    public Person(String firstname, String lastname, int sin) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.sin = sin;
    }
    public Person(String firstname, String lastname) {
        this();
        this.firstname = firstname;
        this.lastname = lastname;
//        setSin(-1);
    }

    @Override
    public String toString() {
        return getFirstname()+" "+getLastname();
    }

    @Override
    public boolean equals(Object obj) {
        // we define equality as follows:
        // 2 Person objects are the same if they have the same SIN
        if(! (obj instanceof Person))
            return false;
        Person p=(Person)obj;
        
        if(
                p.getSin()==this.getSin() && 
                p.getFirstname().equals(this.getFirstname()) && 
                p.getLastname().equals(this.getLastname())
           )
            return true;

//        if(p == this)
//            return true;
        
        return false;
    }

  
    
    /**
     * @return the firstname
     */
    public String getFirstname() {
        return firstname;
    }

    /**
     * @param firstname the firstname to set
     */
    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    /**
     * @return the lastname
     */
    public String getLastname() {
        return lastname;
    }

    /**
     * @param lastname the lastname to set
     */
    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    /**
     * @return the sin
     */
    public int getSin() {
        return sin;
    }

    /**
     * @param sin the sin to set
     */
    public void setSin(int sin) {
        this.sin = sin;
    }
    
}
