/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ____fall2021.lecture4.ex1;

/**
 *
 * @author fcarella
 */
public class AnObject {
    private String aString;
    private boolean aBoolean;
    private int anInt;
    private static int COUNT;

    public AnObject() {
    }

    public AnObject(String aString, boolean aBoolean, int anInt) {
        this.aString = aString;
        this.aBoolean = aBoolean;
        this.anInt = anInt;
    }

    @Override
    public String toString() {
        return super.toString(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * @return the aString
     */
    public String getaString() {
        return aString;
    }

    /**
     * @param aString the aString to set
     */
    public void setaString(String aString) {
        this.aString = aString;
    }

    /**
     * @return the aBoolean
     */
    public boolean isaBoolean() {
        return aBoolean;
    }

    /**
     * @param aBoolean the aBoolean to set
     */
    public void setaBoolean(boolean aBoolean) {
        this.aBoolean = aBoolean;
    }

    /**
     * @return the anInt
     */
    public int getAnInt() {
        return anInt;
    }

    /**
     * @param anInt the anInt to set
     */
    public void setAnInt(int anInt) {
        this.anInt = anInt;
    }

    /**
     * @return the COUNT
     */
    public static int getCOUNT() {
        return COUNT;
    }

    /**
     * @param aCOUNT the COUNT to set
     */
    public static void setCOUNT(int aCOUNT) {
        COUNT = aCOUNT;
    }
    
    
}
