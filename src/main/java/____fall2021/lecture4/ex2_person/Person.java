/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ____fall2021.lecture4.ex2_person;


/**
 *
 * @author fcarella
 */
public class Person implements java.io.Serializable{
    private int age; // in years
    private int height; // in cm
    private String firstname;
    private String lastname;
    private char gender;// M, F, 
    //....
    
    public Person(){
        this.firstname="Default first name";
        this.lastname="Default last name";
    }
    
    public Person(String firstname, String lastname){
//        this.firstname=firstname;
//        this.lastname=lastname;
        setFirstname(firstname);
        setLastname(lastname);
    }

    /**
     * @return the age
     */
    public int getAge() {
        return age;
    }

    /**
     * @param age the age to set
     */
    public void setAge(int age) throws Exception{
        if(age<=0){
            throw new Exception("Error, you cant have a negative or zero age");
        }
        this.age = age;
    }

    /**
     * @return the height
     */
    public int getHeight() {
        return height;
    }

    /**
     * @param height the height to set
     */
    public void setHeight(int height) {
        this.height = height;
    }

    /**
     * @return the firstname
     */
    public String getFirstname() {
        return firstname;
    }

    /**
     * @param firstname the firstname to set
     */
    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    /**
     * @return the lastname
     */
    public String getLastname() {
        return lastname;
    }

    /**
     * @param lastname the lastname to set
     */
    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    /**
     * @return the gender
     */
    public char getGender() {
        return gender;
    }

    /**
     * @param gender the gender to set
     */
    public void setGender(char gender) {
        this.gender = gender;
    }

    @Override
    public String toString() {
        return "["+getFirstname()+" "+getLastname()+"]";
//        return super.toString(); //To change body of generated methods, choose Tools | Templates.
    }

    
}
