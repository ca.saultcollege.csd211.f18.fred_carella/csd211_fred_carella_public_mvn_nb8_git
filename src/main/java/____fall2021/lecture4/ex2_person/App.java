/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ____fall2021.lecture4.ex2_person;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author fcarella
 */
public class App {
    public void run(){
        System.out.println("App is running...");
        
        Person joe=new Person();
        Person noel=new Person();
        
        Person noelHall=new Person("Noel", "Hall");
        Person gilleLarock=new Person("Gilles", "Larock");
        
        joe.setFirstname("Joseph");
        joe.setLastname("Piscopo");
        
        noel.setFirstname("Dennis");
        noel.setLastname("Ochoski");
        
        System.out.println("Joes name="+joe.getFirstname()+" "+joe.getLastname());
        System.out.println("Dennis's name="+noel.getFirstname()+" "+noel.getLastname());
        System.out.println("noel halls name="+noelHall.getFirstname()+" "+noelHall.getLastname());
        System.out.println("gilles name="+gilleLarock.getFirstname()+" "+gilleLarock.getLastname());
        
        System.out.println("Noels name is "+noelHall);
        
        try {
            joe.setAge(23);
        } catch (Exception ex) {
            Logger.getLogger(App.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        
        
    }
}
