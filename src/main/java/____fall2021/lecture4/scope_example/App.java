/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ____fall2021.lecture4.scope_example;

/**
 *
 * @author students
 */
public class App {
    // instance variables
    private int age;
    private String firstname;
    private char gender;
    public static int count;// class variable.  There is only one.

    public App() {
        count++;// gets called every time we instantiate an App object
    }
    
    public void run(){
        int weight;// local variable.  local to the run method
        weight=180;
        
        {
            String eye_color="blue";
        }
        
//        String eye_color="Brown";
        
        for(int i=0;i<10;i++){
            String var="";
        }
//        var="";
    }
    
    private void setGender(char gender){
        this.gender=gender;
    }

    /**
     * @return the age
     */
    public int getAge() {
        return age;
    }

    /**
     * @param age the age to set
     */
    public void setAge(int age) {  
//age parameter is a variable local to the setAge method            
        this.age = age;// this.age refers to the instance variable
                       // age refers to the local variable
    }

    /**
     * @return the firstname
     */
    public String getFirstname() {
        return firstname;
    }

    /**
     * @param firstname the firstname to set
     */
    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }
    
}
