/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ____fall2021.lecture4.scope_example;

/**
 *
 * @author students
 */
public class Main {
    public static void main(String[] args){
        App app1=new App();
        App app2=new App();
        App app3=new App();
        app1.setFirstname("Joe");
        app2.setFirstname("George");
        System.out.println("count="+App.count);
        app1.run();
    }
}
