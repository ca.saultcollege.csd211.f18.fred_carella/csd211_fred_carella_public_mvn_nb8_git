/*
 * create a properly formatted java bean
 * a bean has:
 * - private attributes (instance variables)
 * - public setters and getters (follow naminmg conventions)
 * - have a no argument constructor
 * - make it serialzable
 * ===============================
 * -its good practise to include a static int variable called count, 
 * that you increment in the constructor
 * - also a good practise to utilize exceptions to handle errors.  see code
 * - add a toString method
 */
package ____fall2021.lecture4.ex1_person;

/**
 *
 * @author students
 */
public class Person extends Object implements java.io.Serializable {  // make it serialzable
    // instance variables
    private int age;
    private String firstname;
    
    // class variables
    private static int count;
    
    // uniquely identify a person by his/her SIN number
    private long sin;
    
    
    // no argument constructor
    public Person(){
        count++;
        System.out.println("Creating a person #"+count);
    }
    
    public boolean equals(Object o){
        if(this == o)
            return true;
        
        if(! (o instanceof Person))
            return false;
        Person p=(Person)o;// cast o to a person
        if(p.sin == this.sin)
            return true;
        return false;
    }
    
    public Person(String firstname){
        this.firstname=firstname;
    }
    
    public Person(String firstname, int age) throws Exception{
        this.firstname=firstname;
        setAge(age);
    }
    
    public Person(String firstname, int age, long sin) throws Exception{
        this.firstname=firstname;
        setAge(age);
        this.sin=sin;
    }
    
    public int getAge(){
        return age;
    }
    public void setAge(int age) throws Exception {
        if(age <= 0){
            // problem, handle it
//            System.out.println("negative and aero ages, not allowed");
            throw new Exception("negative and zero ages, not allowed");
        }
        this.age=age;// set the value of the iunstance variable "age" to
                     // the value of the parametre called age
    }
    public String getFirstname(){
        return firstname;
    }
    public void setFirstname(String firstname){
        this.firstname=firstname;
    }

    /**
     * @return the count
     */
    public static int getCount() {
        return count;
    }

    /**
     * @param aCount the count to set
     */
    public static void setCount(int aCount) {
        count = aCount;
    }
    
    @Override
    public String toString(){
        return "Hi my name is "+ getFirstname()+" and I am "+getAge()+" years old";
    }

    /**
     * @return the sin
     */
    public long getSin() {
        return sin;
    }

    /**
     * @param sin the sin to set
     */
    public void setSin(long sin) {
        this.sin = sin;
    }
    
}
