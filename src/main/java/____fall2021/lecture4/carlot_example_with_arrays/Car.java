/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ____fall2021.lecture4.carlot_example_with_arrays;

/**
 *
 * @author fcarella
 */
public class Car {
    private String make;
    private Person owner;

    public Car() {
    }

    public Car(String make) {
        this.make = make;
    }

    @Override
    public String toString() {
        return getMake();
    }
    

    /**
     * @return the make
     */
    public String getMake() {
        return make;
    }

    /**
     * @param make the make to set
     */
    public void setMake(String make) {
        this.make = make;
    }

    /**
     * @return the owner
     */
    public Person getOwner() {
        return owner;
    }

    /**
     * @param owner the owner to set
     */
    public void setOwner(Person owner) {
        this.owner = owner;
    }
    
}
