/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ____fall2021.lecture4.carlot_example_with_arrays;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 *
 * @author fcarella
 */
public class App {

    private final Car[] carLot = new Car[100];
    private final Car[] soldCars = new Car[100];
    private int carIndex = 0;
    private Scanner in;


    public void run() {
        boolean done = false;
        String menuString = "Simple Calculator" + "\n"
                + "1. Add a car" + "\n"
                + "2. List all the cars" + "\n"
                + "3. Delete a Car" + "\n"
                + "4. Edit a Car" + "\n"
                + "5. Sell a Car" + "\n"
                + "99. Exit" + "\n"
                + "Please enter the operation to perform (1, 2, 3 or 4) : " + "\n";
        in = new Scanner(System.in);
        while (!done) {
            System.out.println("" + menuString);
            try {
                int choice = in.nextInt();

                switch (choice) {
                    case 1:
                        addCar();
                        break;
                    case 2:
                        listCars();
                        break;
                    case 3:
//                        mutltiply();

                        break;
                    case 4:
//                        divide();
                        edit();
                        break;
                    case 5:
                        sellCar();
                        break;
                    case 99:
                        done = true;
                        break;
                    default:
                        System.out.println("You chose incorrectly");

                }
            } catch (InputMismatchException e) {
                System.out.println("You entered an incorrect option, please try again.");
            } catch (UnsupportedOperationException e) {
                System.out.println("Unsupported operation.");
            }
        }
        System.exit(0);
    }

    private void addCar() {
        in = new Scanner(System.in);
        System.out.println("What is the make:");
        String make = in.next();
        Car car = new Car(make);
        carLot[carIndex++] = car;
//        carIndex++;
        System.out.println("Added the Car [" + car + "]");
    }

    private void listCars() {
        System.out.println("\n\n\n----------------Cars List--------------------------");
        int index = 0;
//        for(index=0;index<cars.length;index++){
//            if(cars[index]==null)
//                continue;
//            System.out.println( (index+1)+". "+cars[index]);
//        }
        // for each loop
        index = 0;
        for (Car aCar : carLot) {
            if (aCar == null) {
                continue;
            }
            System.out.println((++index) + ". " + aCar);

        }
        System.out.println("---------------------------------------------------\n\n\n");
    }

    private void sellCar() {
        // find out which car
        listCars();
        in = new Scanner(System.in);
        System.out.println("Which car do you want to sell");
        int choice = in.nextInt();
        choice--;
        Car car2sell = carLot[choice];

        // create a new Owner
        Person owner = new Person();
        owner.setFirstname("Joe");
        owner.setLastname("Student");
        // attach an owner to the car, thus "selling it" (indicating its sold)
        car2sell.setOwner(owner);

        // delete the car from the carlot 
        soldCars[0] = car2sell;
        carLot[choice] = null;

    }

    private void edit() {
        listCars();
        System.out.println("Which car would you like to edit ?:");
        int choice = in.nextInt();
        in = new Scanner(System.in); // reset the scanner
        if (choice > 0) {
            Car c = carLot[choice - 1];
            System.out.println("Make: " + c.getMake());
            c.setMake(getInput(c.getMake()));
//            System.out.println("Model: " + c.getModel());
//            c.setModel(getInput(c.getModel()));
//            System.out.println("Year: " + c.getYear());
//            c.setYear(getInput(c.getYear()));
        } else {
            System.out.println("Choice out of bounds");
        }
        System.out.println("");
    }

    private String getInput(String s) {
        String ss = in.nextLine();
        if (ss.trim().isEmpty()) {
            return s;
        }
        Scanner in2 = new Scanner(ss);
        return in2.nextLine();
    }

    private int getInput(int i) {
        String s = in.nextLine();
        if (s.trim().isEmpty()) {
            return i;
        }
        Scanner in2 = new Scanner(s);
        return in2.nextInt();
    }

    private double getInput(double i) {
        String s = in.nextLine();
        if (s.trim().isEmpty()) {
            return i;
        }
        Scanner in2 = new Scanner(s);
        return in2.nextDouble();
    }

}
