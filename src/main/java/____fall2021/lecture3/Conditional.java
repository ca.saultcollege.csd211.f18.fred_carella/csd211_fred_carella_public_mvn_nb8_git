/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ____fall2021.lecture3;

import java.net.SocketOptions;

/**
 *
 * @author fcarella
 */
public class Conditional {
    public static void main(String[] args) {
        int x=10;
        x=11;
        //////////
        String s;
        if(x==10){
            s=("x==10");
        }else{
                s=("x!=10");
        }
        System.out.println(s);
        ///////////
        
        s=(x==10)?"x==10":"x!=10";
        System.out.println(s);
    }
    
}
