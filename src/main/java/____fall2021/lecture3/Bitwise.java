/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ____fall2021.lecture3;

import ___fall2020.lecture3.*;

/**
 *
 * @author fcarella
 */
public class Bitwise {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        char num=0x43;// 0x43=0100011
        int shiftLeft = num<<1;
        System.out.println("0x43 shifted to the left once = "+Integer.toBinaryString(new Integer(shiftLeft)));
        int shiftRight = shiftLeft>>2;
        System.out.println("0x43 shifted to the left once then twice to the right is = "+Integer.toBinaryString(new Integer(shiftRight)));

    }

}
