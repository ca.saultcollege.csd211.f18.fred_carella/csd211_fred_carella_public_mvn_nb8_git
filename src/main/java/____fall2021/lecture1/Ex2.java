package ____fall2021.lecture1;

public class Ex2{
    public static void main(String[] yyy){
        System.out.println("Ex2-->");
        
        // john is an instance of a Person class
        Person john=new Person();
        john.firstname="John";
        System.out.println(john.firstname);
        
        // peter is annother instance of a person class
        Person peter=new Person();
        peter.firstname="Peter";
        System.out.println(peter.firstname);
        
        Person pete=peter;
        
        // peter and john are "references" to Person objects
        john=null;
        System.gc();
//        john.firstname="John";
//        System.out.println(john.firstname);
        
        
    }
}
    
