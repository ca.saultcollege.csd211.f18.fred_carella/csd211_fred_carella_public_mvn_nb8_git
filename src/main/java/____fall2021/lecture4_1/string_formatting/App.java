/*
 * Demonstrate String formatters
 * See https://dzone.com/articles/java-string-format-examples?edition=625303&utm_medium=email&utm_source=dzone&utm_content=Java%20string%20format%20examples&utm_campaign=
 */
package ____fall2021.lecture4_1.string_formatting;

import ___fall2020.lecture4_1.string_formatting.*;
import java.util.Formatter;

/**
 *
 * @author students
 */
public class App {

    void run() {
        //
        System.out.println("Demonstrate String formatters...");
        //
        int aNum = 10;
        System.out.println("The number is " + aNum);

        String output = String.format("%s = %d", "joe", 35);
        System.out.println(output);

        System.out.printf("%s = %d", "joe", 35);

        StringBuilder sbuf = new StringBuilder();
        Formatter fmt = new Formatter(sbuf);
        fmt.format("\n\nPI = %f\n", Math.PI);
        System.out.print(sbuf.toString());
        // you can continue to append data to sbuf here.

        // specify a width
        System.out.printf("\n\n|%20d|", 93); // prints: |                  93|
        // Left-justifying within the specified width:
        String f = String.format("\n\n|%-20d|", 93); // prints: |93                  |
        System.out.printf(f);
        // Pad with zeros:
        f=String.format("\n\n|%020d|", 93); // prints: |00000000000000000093|
        System.out.printf(f);


    }

}
