/*
 * Demonstrate String formatters
 * See https://dzone.com/articles/java-string-format-examples?edition=625303&utm_medium=email&utm_source=dzone&utm_content=Java%20string%20format%20examples&utm_campaign=
 */
package ____fall2021.lecture4_1.string_formatting;

import ___fall2020.lecture4_1.string_formatting.*;

/**
 *
 * @author students
 */
public class Main {
    public static void main(String[] args){
        new App().run();
    }
}
