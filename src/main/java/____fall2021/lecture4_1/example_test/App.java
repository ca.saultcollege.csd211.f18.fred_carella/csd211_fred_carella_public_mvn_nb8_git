/*
 * The online/lecture example uses %n instead of \n test out which works
 */
package ____fall2021.lecture4_1.example_test;

import java.util.Formatter;

/**
 *
 * @author fcarella
 */
public class App {

    public App() {
    }

    public void run() {
        StringBuilder sbuf = new StringBuilder();
        Formatter fmt = new Formatter(sbuf);
        fmt.format("PI = %f%nNext line", Math.PI);// %n is an error/typo
        fmt.format("PI = %f\nNext line", Math.PI);// should be \n 
        System.out.print(sbuf.toString());
// you can continue to append data to sbuf here.
    }

}
