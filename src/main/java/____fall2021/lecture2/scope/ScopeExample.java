/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ____fall2021.lecture2.scope;

/**
 *
 * @author students
 */
public class ScopeExample {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // local variables
        Person jack=new Person(); // create an instance of a Person class called joe
        Person jill= new Person();// create an instance of a Person class called jill
        // joe and jill are objects of type Person.  They each have a firstname
        jack.setFirstname("Jack");
        jill.setFirstname("Jill");
//        jack.firstname="Jack";
        
        jack.COUNT++;
        jill.COUNT++;
        
        System.out.println("Jack=="+jack.getFirstname());
        System.out.println("Jill=="+jill.getFirstname());
        
        System.out.println("COUNT="+Person.COUNT);
        System.out.println("COUNT="+jill.COUNT);
        System.out.println("COUNT="+jack.COUNT);
        
    }
    
}
