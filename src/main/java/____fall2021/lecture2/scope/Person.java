/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ____fall2021.lecture2.scope;

/**
 *
 * @author students
 */
public class Person {
    
    private String firstname; // firstname is a private instance variable
                              // every instance of a Person class will have its own firstname
    public static int COUNT;  // public class variable.  Theres only one COUNT variable for all instances of a class


    /**
     * @return the firstname
     */
    public String getFirstname() {
        int y=COUNT; // y is a local variable.  Its local to the getFirstname method
        return firstname;
    }

    /**
     * @param firstname the firstname to set
     * firstname ==  a parameter whose scope is local to the setFirstname method
     */
    public void setFirstname(String firstname) {
//        int x=y;  // y is out of scope.  you cant see it here because its a local variable in the getFirstname method;
        this.firstname = firstname;
    }

    
}
