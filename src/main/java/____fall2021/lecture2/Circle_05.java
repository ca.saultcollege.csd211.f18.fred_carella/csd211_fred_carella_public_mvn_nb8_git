/*
 * Get the inputs from the users keyboard input using a scanner.  
 * Use the Math package for value of PI. Draw a circle in a JFrame.
 */
package ____fall2021.lecture2;

import ___fall2020.lecture2.*;
import java.util.Scanner;

/**
 *
 * @author fcarella
 */
public class Circle_05 {
    private static int radius; // instance variable radius
    private final static double PI=3.14;// instance variable PI.  
                                        // Because PI is a constant we declare it final, meaning you cant change it once its initialized the first time
    private static int centerX;
    private static int centerY;
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        // input
        Scanner input=new Scanner(System.in);
        System.out.println("Enter the radius of the circle: ");// prompt the user            
        radius=input.nextInt();// get the radius from the keyboard
        System.out.println("Enter center X of circle: ");// prompt the user            
        centerX=input.nextInt();// get the radius from the keyboard
        System.out.println("Enter center Y of circle: ");// prompt the user            
        centerY=input.nextInt();// get the radius from the keyboard
        
        // process
        double area2=Math.pow(radius, 2)*Math.PI;// method 2 : calculate area using the Math package methods
        
        // output
        System.out.println("Method 2 : The area of a circle with the radius "+radius+" = "+area2);
        Circle_05_jframe circle = new Circle_05_jframe();
        circle.setX(centerX);
        circle.setY(centerY);
        circle.setRadius(radius);
        circle.setText("Method 2 : The area of a circle with the radius "+radius+" = "+area2);
        
    }
}
