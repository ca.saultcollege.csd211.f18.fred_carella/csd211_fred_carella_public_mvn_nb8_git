/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ____fall2021.lecture2;

/**
 *
 * @author students
 */
public class Ex1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // declaring 2 variables called x and y
        // variables have type
        // variables in java are statically type
        // int and double are what is known as primitive data types
        //   they take up space in memory
        // primnitive data type start with a lowercase character
        int x;
        double y;

        x = 10; // x is an integer (has no fractional part)
        y = 10.0d; // y is a double which is a real number (has a fractional part)
        // 10d == a real number of type double

        y = 10; // java will convert the integer 10 int a double and then assign it to y

        int aPrimitiveInt = 12;// a primitive integer
        Integer anObjectInteger = new Integer(10);

        int z = aPrimitiveInt + anObjectInteger;
    }

}
