/*
 
 * A Simple program showing variable declarations and initializations.  
 * An example is given for all primitive data types.
 * Ranges for numeric types are given in comments.
 
 */

package ____fall2021.lecture2;

import ___fall2020.lecture2.*;

public class Variables_02 {

    public static void main(String[] args) {
// all of these variables are local variables.

// declarations
        
// 32-bit number. Range -2,147,483,648 to  2,147,483,647
        int anInteger; 
        int anInteger2;
        int a, b, c;
        
// range for double (64 bit floating point #) and float (32 bit floating point #) is really big :  
// Emin = -(2^(K-1) -2) and Emax = 2^(K-1)-1, inclusive, and where N and K are parameters that depend on the value set
        double aDouble;
        float aFloat;
// 16 bit # range = -32,768 to 32,767
        short aShort; 
//  8-bit # range = -128 to 127
        byte aByte; 
// 64-bit # range = -9,223,372,036,854,775,808 to 9,223,372,036,854,775,807
        long aLong;
// 16-bit Unicode character. range =  '\u0000' (or 0) to '\uffff' (or 65,535 inclusive).
// see : see http://http://en.wikipedia.org/wiki/List_of_Unicode_characters
        char aChar;
// is either true or false
        boolean aBoolean;


// declare and initialize
        int xx = 10;
        int yy = 20;
//        int yy = 1.2; // THIS IS AN ERROR, 1.2 is a REAL number not an INT!
        int aa = 10, bb = 20, cc=30;


// initializations.
        anInteger = 20;
        aDouble = 34.67;
        aFloat = 34.67f;
        aByte = 127;
//        aByte = 128;
        System.out.println("byte = 127.  Does it ? " + aByte);
        
        aShort=-32768;
        aShort=32767;
        
        aLong=9223372036854775807l; // thats a BIG number!
        
        aChar='\u0041'; // 0041 == 'A'     see http://http://en.wikipedia.org/wiki/List_of_Unicode_characters
        System.out.println(aChar);
        
        aBoolean=true;
        aBoolean=false;
    }
}
