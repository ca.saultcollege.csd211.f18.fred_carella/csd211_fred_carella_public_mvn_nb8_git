/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ___fall2020.lecture1;

/**
 *
 * @author students
 */
class MyClass{
    int x;
    public String convert2Binary() {
        return "";
    }
}
public class Ex1 {
    
    public static void main(String[] args) {
        int x=10; // integer variable called x
               // this is a primitive data type a
        Integer yObject=new Integer(20);
        Integer zObject=new Integer(30);
        Integer gObject=50;
        
        int thisIsAnInteger; // camel hump naming convention.
        int this_is_an_integer; // ?? underscore naming convention
        
        String firstName="Fred"; // create a reference to a String object 
                                 // called firstName (String is the class)
                                 // and firstName is a reference to the String Object
                                 // whose value is "Fred"
        String lastName=new String("Carella"); // lastName is a reference to 
                                               // an instance of a String Object
        Double doubleX=new Double(12.3);
        Float floatY=new Float(9.0);
        Float floatX=new Float(12.8);
        
        Double aDouble=12.3;
        
        MyIntegerClass myIntegerX=new MyIntegerClass(); // create an object based on my own class;
        
                                 
        MyClass mc=new MyClass();
        
        mc=null; // deletes the MyClass object called mc.
                 // Note, the garbage collector will clean up the memory leak
        
        System.gc();// not necessary but you can force garbage collectio with
                    // this command;
        
        
        
        System.out.println("x="+x);
        
        Integer xObject=new Integer(10); // an object of type Integer
        System.out.println("xObject="+xObject);
        System.out.println("xObject.floatValue="+xObject.floatValue());
        
        xObject=null;
        
        System.out.println("xObject.floatValue="+xObject.floatValue());
        System.out.println("xObject="+xObject);
        
        
        System.out.println(aDouble);
        
        
    }
    
}
