/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ___fall2020.test3_review;

/**
 *
 * @author students
 */
public class Camera {
    // create private instance variable attributes
    private String make;
    private String model;
    private long serialNumber;
    // class variable
    private static int COUNT;
    
    public Camera(){ // no argument constructor
        // constructors are called when you instantiate an object
        make="default";
        model="default";
        serialNumber=12345l;
        COUNT++;
    }
    
    @Override
    public boolean equals(Object o){
        if(!(o instanceof Camera)){
            return false;
        }
        if(o==this){
            return true;
        }
        Camera camera=(Camera)o;
        if(camera.getSerialNumber()==this.getSerialNumber())
            return true;
        return false;
    }

    @Override
    protected void finalize() throws Throwable {
        try {
            COUNT--;
        } finally {
            super.finalize();
        }
    }
    
    
    
    @Override
    public String toString(){
        return "["+make+","+model+","+serialNumber+"]";
    }
    

    public Camera(String make, String model, long serialNumber) {
        this.make = make;
        this.model = model;
        this.serialNumber = serialNumber;
        COUNT++;
    }
    
    
    public String getMake(){
        return make;
    }

    /**
     * @param make the make to set
     */
    public void setMake(String make) {
        this.make = make;
    }

    /**
     * @return the model
     */
    public String getModel() {
        return model;
    }

    /**
     * @param model the model to set
     */
    public void setModel(String model) {
        this.model = model;
    }

    /**
     * @return the serialNumber
     */
    public long getSerialNumber() {
        return serialNumber;
    }

    /**
     * @param serialNumber the serialNumber to set
     */
    public void setSerialNumber(long serialNumber) {
        this.serialNumber = serialNumber;
    }

    /**
     * @return the COUNT
     */
    public static int getCOUNT() {
        return COUNT;
    }

    /**
     * @param aCOUNT the COUNT to set
     */
    public static void setCOUNT(int aCOUNT) {
        COUNT = aCOUNT;
    }
    
}
