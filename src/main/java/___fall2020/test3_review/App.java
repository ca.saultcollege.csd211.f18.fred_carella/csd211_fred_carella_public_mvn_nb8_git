/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ___fall2020.test3_review;

import java.util.Scanner;
import lab4.q2.Car;

/**
 *
 * @author students
 */
public class App {
    Camera[] cameras=new Camera[10];
    
    Scanner in = new Scanner(System.in);

    public void run() {
        System.out.println("Camera App");
        Camera rebel=new Camera("Canon", "Rebel", 12345l);
        Camera rebel2=new Camera("Canon", "Rebel", 12345l);
        Camera fuji=new Camera("Fuji", "Finepix S520", 34567l);
        Camera pro=rebel;// pro and rebel are references.
        
        
        System.out.println(rebel);
        System.out.println(fuji);
        System.out.println("Number of cameras="+Camera.getCOUNT());
        fuji=null;
        System.gc();
        System.out.println("Number of cameras="+Camera.getCOUNT());
        
        if(pro == rebel){ // yes works
            System.out.println("They're equal");
        }
        Car mustang=new Car();
        if(rebel.equals(mustang)){
            System.out.println("They're equal");
        }else{
            System.out.println("They're NOT equal");
        }
        if(rebel.equals(pro)){ // should be false
            System.out.println("They're equal");            
        }else{
            System.out.println("They're NOT equal");
        }
        if(rebel2.equals(pro)){ // should be false
            System.out.println("They're equal");            
        }else{
            System.out.println("They're NOT equal");
        }

        boolean done = false;
        while (!done) {
            System.out.println("1. Add camera");
            System.out.println("2. List camera");
            System.out.println("3. Edit camera");
            System.out.println("99. Exit");
            System.out.println("Enter choice: ");
            try {
                int choice = in.nextInt();
                switch (choice) {
                    case 1:
                        add();
                        break;
                    case 2:
                        list();
                        break;
                    case 3:
                        edit();
                        break;
                    case 99:
                        done = true;
                        break;
                    default:
                        System.out.println("Wrong choice, try again");
                }
            } catch (Exception e) {
                in=new Scanner(System.in);
                System.out.println("Bad choice, try again!");
            }
        }

    }

    public void add() {
        System.out.println("Add");
    }

    public void add(Camera c) {
        System.out.println("Added "+c);
    }

    public void list() {
        System.out.println("List");
    }

    public void edit() {
        System.out.println("Edit");
    }
    
    public Camera findCamera(Camera camera){
        return new Camera();
//        return null;
    }
}
