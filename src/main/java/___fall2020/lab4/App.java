/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ___fall2020.lab4;

import java.util.Scanner;

/**
 *
 * @author students
 */
public class App {
    private Car[] carLot=new Car[100];
    private int carIndex=0;
    private Scanner input=new Scanner(System.in);
    
    public void run() {
        System.out.println("Lab 4");
        boolean done=false;
        
        while(!done){
            System.out.println("1. Add car");
            System.out.println("2. List cars");
            System.out.println("99. Add car");
            System.out.println("Make a choice:");
            
            int choice=input.nextInt();
            
            switch(choice){
                case 1: addCar();
                break;
                case 2: listCars();
                break;
                case 99: done=true;
                default:
                    System.out.println("Bad choice try again");
            }
        }
    }

    private void addCar() {
        Car car=new Car();
        car.setMake("Ford");
        Car car2=new Car();
        car2.setMake("Chevy");
        carLot[carIndex++]=car;
        carLot[carIndex++]=car2;
    }

    private void listCars() {
        System.out.println("There are "+Car.getCOUNT()+" cars in the lot");
        for(int i=0;i<carLot.length;i++){
            if(carLot[i]==null)
                break;
            System.out.println((i+1)+". Car = "+carLot[i].getMake());
        }
    }
    
}
