/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ___fall2020.lab4;

/**
 *
 * @author students
 */
public class Car {
    private String make;
    private static int COUNT;

    public Car() {
        COUNT++;
    }
    

    
    /**
     * @return the make
     */
    public String getMake() {
        return make;
    }

    /**
     * @param make the make to set
     */
    public void setMake(String make) {
        this.make = make;
    }

    /**
     * @return the COUNT
     */
    public static int getCOUNT() {
        return COUNT;
    }

    /**
     * @param aCOUNT the COUNT to set
     */
    public static void setCOUNT(int aCOUNT) {
        COUNT = aCOUNT;
    }
    
}
