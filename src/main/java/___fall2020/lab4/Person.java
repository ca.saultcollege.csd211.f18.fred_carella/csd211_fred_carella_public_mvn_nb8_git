/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ___fall2020.lab4;

/**
 *
 * @author students
 */
public class Person {
    // instance variables
    private String firstname;
    private String lastname;
    // class variables
    private static int COUNT; 

    public Person() {
        COUNT++;
    }

    @Override
    public String toString() {
        return firstname+" "+lastname;
    }
    
    
    
    
    public String getFirstname(){
        return firstname;
    }
    public void setFirstname(String firstname){
        this.firstname=firstname;
    }

    /**
     * @return the lastname
     */
    public String getLastname() {
        return lastname;
    }

    /**
     * @param lastname the lastname to set
     */
    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    /**
     * @return the COUNT
     */
    public static int getCOUNT() {
        return COUNT;
    }

    /**
     * @param aCOUNT the COUNT to set
     */
    public static void setCOUNT(int aCOUNT) {
        COUNT = aCOUNT;
    }
    
}
