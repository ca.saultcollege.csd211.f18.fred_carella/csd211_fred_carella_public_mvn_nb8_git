/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ___fall2020.lecture2;
import java.awt.Graphics;
import java.awt.Color;
import javax.swing.JFrame;

/**
 *
 * @author fcarella
 */
public class Circle_05_jframe extends JFrame {
    private int x,y;
    private int radius;
    private String text;
    public Circle_05_jframe() {
        //Set JFrame title
        super("Draw A Circle In JFrame");

        //Set default close operation for JFrame
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        //Set JFrame size
        setSize(400, 400);

        //Make JFrame visible 
        setVisible(true);
    }

    public void paint(Graphics g) {
        super.paint(g);

        //draw circle outline
        g.drawOval(getX(), getY(), getRadius(), getRadius());

        //set color to RED
        //So after this, if you draw anything, all of it's result will be RED
        g.setColor(Color.RED);

        //fill circle with RED
        g.fillOval(getX(), getY(), getRadius(), getRadius());
        
        g.drawString(text, 100, 100);
    }

    public static void main(String[] args) {
        Circle_05_jframe frame = new Circle_05_jframe();
    }

    /**
     * @return the x
     */
    public int getX() {
        return x;
    }

    /**
     * @param x the x to set
     */
    public void setX(int x) {
        this.x = x;
    }

    /**
     * @return the y
     */
    public int getY() {
        return y;
    }

    /**
     * @param y the y to set
     */
    public void setY(int y) {
        this.y = y;
    }

    /**
     * @return the radius
     */
    public int getRadius() {
        return radius;
    }

    /**
     * @param radius the radius to set
     */
    public void setRadius(int radius) {
        this.radius = radius;
    }

    void setText(String string) {
        text=string;
        repaint();
    }
}
