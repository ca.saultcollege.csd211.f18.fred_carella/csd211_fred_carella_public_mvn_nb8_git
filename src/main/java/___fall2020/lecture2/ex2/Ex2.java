/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ___fall2020.lecture2.ex2;

import java.util.Scanner; // ctrl-shift i in netbaens to get the imports




/**
 *
 * @author students
 */
public class Ex2 {
    public static void main(String[] args) {
        // input
        int intData1=10;// declare and initialize: HARD CODE THE DATA
        int intData2=23;
        // process
        double sum=intData1+intData2;
        
        // output
        System.out.println("sum="+sum);// output to the console (standard output)
        
        
        // get input from the keyboard
        Scanner input=new Scanner(System.in);
        System.out.println("Enter a value for intData1:");
        intData1=input.nextInt();
        System.out.println("Enter a value for intData2:");
        intData2=input.nextInt();
        // process
        sum=intData1+intData2;
        // output
        System.out.println("sum="+sum);// output to the console (standard output)
        
        
        System.out.println("This is the first string");
        System.out.println("This is the second string");
        
        System.out.print("This is the first string");
        System.out.print("This is the second string");
        
        
        System.out.printf("\nThe sum of %5d = %5d + %5d",(int)sum,intData1,intData2);
        System.out.printf("\nThe sum of %5d = %5d + %5d",(88+99),88,99);
        
    }
}
