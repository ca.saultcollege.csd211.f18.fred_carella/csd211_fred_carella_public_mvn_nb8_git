/*
 * Lecture 2 Exercies 1:
 * Ex1 -
 * demonstrate variables and variable scope
 */
package ___fall2020.lecture2.ex1;

/**
 *
 * @author students
 */
public class Ex1 {
    
    public static void main(String[] args){ // args is a parameter which is local to the main class
//        int x=10; // this is a local variable
//                  // the variable is local to the main method
//        // is args a local variable???
//        // yes, args is a variable thats local to the main method
//        // its not available to any other method
//        float real_1=12.3847328748927314f; // declare and initialize
//        double real_2=12.0d; // declare and initialize
//        double real_3;// declare 
//        byte aByte=127;// 8 bit numbers
//        byte aByte2=-128;
////        real_1=12.3;// trying to put a double into a float, thats an error.
//        real_1=12.3f;// 12.3 is normnally treated like a double but by appending an"f"
//                     // we can tell java to treat it like a float
//        real_1=(float)12.3;// cast or convert 12.3 from a double to a float and then assign to a float variable
//        
//        int sum;
//        int numerator=3;
//        int denominator=2;
//        sum=numerator/denominator; // sum should be 1.5
//                                   // in java, sum will be truncated, that is the
//                                   // fractional part is lost, and summ will equal 1.
//                                   // we lose the .5
//        
//                                   
//        double sum2;
//        int numerator2=3;
//        int denominator2=2;
//        sum2=(double)numerator2/denominator2; // we expect 1.5
//        int y=10;
//        
//        
//        Double dd=new Double(12.8); // first instance of a Object of type Double
//        Double dd2=new Double(12.5); // second instance of a Object of type Double
//      
        System.out.println("MyClass.COUNT="+MyClass.COUNT);

        MyClass m1=new MyClass();
        System.out.println("m1.COUNT="+m1.COUNT);
        MyClass m2=new MyClass();
        System.out.println("m2.COUNT="+m2.COUNT);
        
        System.out.println("m1.COUNT"+m1.COUNT); // m1.COUNT and m2.COUNT are the SAME variable because
                                                 // COUNT is a class variable
                                                 
        
        
        
        System.out.println("m1.x="+m1.getX());
        System.out.println("m2.x="+m2.getX());
        m2.setX(18);
        System.out.println("m1.x="+m1.getX());
        System.out.println("m2.x="+m2.getX());
        
    }
    
    private static int getInt(){
//        String name=abcd[0];
        double sum2=10.3;
        return 5;
    }
}
