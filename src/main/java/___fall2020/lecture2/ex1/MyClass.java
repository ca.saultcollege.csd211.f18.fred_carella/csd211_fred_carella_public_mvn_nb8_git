/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ___fall2020.lecture2.ex1;

/**
 *
 * @author students
 */
public class MyClass {
    
    public static int COUNT=0;// this is a class variable;
                              // class variable is accessible to every instance of a class.
                              // there is only ONE COPY of COUNT in the application
    private int x=10; // instance variable (non static fields)
                      // x is defined inside 
                      // the outer class so that makes it a an instance variable

    public MyClass() {
        COUNT++;
    }
    
    public int getX(){
        int y=9;// 
        return x;
    }
    public void setX(int x){ // x is a parameter for the setX method
        this.x=x;
    }
    
}
