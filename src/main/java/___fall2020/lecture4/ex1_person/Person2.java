/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ___fall2020.lecture4.ex1_person;

/**
 *
 * @author students
 */
public class Person2 {
    private int age;
    private String firstname;
    private String lastname;
    private int height;

    /**
     * @return the age
     */
    public int getAge() {
        return age;
    }

    public Person2() {
    }

    public Person2(int age, String firstname, String lastname, int height) {
        this.age = age;
        this.firstname = firstname;
        this.lastname = lastname;
        this.height = height;
    }
    public Person2(int age, String firstname, String lastname) {
        this.age = age;
        this.firstname = firstname;
        this.lastname = lastname;
    }
    public Person2(int age, String firstname) {
        this.age = age;
        this.firstname = firstname;
    }
    public Person2(String firstname) {
        this.firstname = firstname;
    }

    /**
     * @param age the age to set
     */
    public void setAge(int age) {
        this.age = age;
    }

    /**
     * @return the firstname
     */
    public String getFirstname() {
        return firstname;
    }

    /**
     * @param firstname the firstname to set
     */
    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    /**
     * @return the lastname
     */
    public String getLastname() {
        return lastname;
    }

    /**
     * @param lastname the lastname to set
     */
    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    /**
     * @return the height
     */
    public int getHeight() {
        return height;
    }

    /**
     * @param height the height to set
     */
    public void setHeight(int height) {
        this.height = height;
    }
    
    
}
