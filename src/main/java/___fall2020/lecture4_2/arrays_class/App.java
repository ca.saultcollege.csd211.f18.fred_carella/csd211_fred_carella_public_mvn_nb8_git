/*
 * Demonstrate the Arrys helper class
 * See https://www.geeksforgeeks.org/array-class-in-java/
 * for more examples
 */
package ___fall2020.lecture4_2.arrays_class;

import java.util.Arrays;

/**
 *
 * @author students
 */
public class App {

    public void run() {
        // Get the Array 
        {
            int intArr[] = {10, 20, 15, 22, 35};

            // To convert the elements as List 
            System.out.println("Integer Array as List: "
                    + Arrays.asList(intArr));
        }
        // searches for the specified element in the 
        // array with the help of Binary Search algorithm.
        {
            int intArr[] = {10, 20, 15, 22, 35};

            // array must be sorted first
            Arrays.sort(intArr);

            int intKey = 22;

            System.out.println(intKey
                    + " found at index = "
                    + Arrays
                            .binarySearch(intArr, intKey));
        }

        {
//      This method searches a range of the specified array for the 
//      specified object using the binary search algorithm.
            int intArr[] = {10, 20, 15, 22, 35};

            Arrays.sort(intArr);

            int intKey = 22;

            System.out.println(
                    intKey
                    + " found at index = "
                    + Arrays
                            .binarySearch(intArr, 1, 4, intKey));
        }

        /*This method checks if both the arrays are equal or not*/
        {
            int intArr[] = {10, 20, 15, 22, 35};

            // Get the second Arrays 
            int intArr1[] = {10,  15, 20, 22, 35};

            // To compare both arrays 
            System.out.println("Integer Arrays on comparison: "
                    + Arrays.equals(intArr, intArr1));
            // Output == Integer Arrays on comparison: false
        }


        /*
This method compares two arrays passed as parameters lexicographically.
         */
        {
            int intArr[] = {10, 20, 15, 22, 35};

            // Get the second Array 
            int intArr1[] = {10, 15, 22};

            // To compare both arrays 
            /*
            outputs 1:
            Outputs:  
            * 0 if the first and second array are 
            equal and contain the same elements in the same 
            order; 
            * less than 0 if the first array is 
            lexicographically less than the second array; 
            * a value greater than 0 if the first array is 
            lexicographically greater than the second array
             */
            System.out.println("Integer Arrays on comparison: "
                    + Arrays.compare(intArr, intArr1));
            
        }
    }
}

