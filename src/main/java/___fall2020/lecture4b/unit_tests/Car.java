/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ___fall2020.lecture4b.unit_tests;

/**
 *
 * @author students
 */
public class Car {

    private String make;
    private String model;
    private int year;
    private long vin;

    public Car(String make, String model, int year, long vin) {
        this.make = make;
        this.model = model;
        this.year = year;
        this.vin=vin;
    }


    @Override
    public boolean equals(Object obj) {
        if(!(obj instanceof Car))
            return false;
        Car car=(Car)obj;
        // return true if equal, false if not
        return car.getVin()==this.getVin();
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + (int) (this.vin ^ (this.vin >>> 32));
        return hash;
    }

//    @Override
//    public int hashCode() {
//        return super.hashCode(); //To change body of generated methods, choose Tools | Templates.
//    }
    
    

    @Override
    public String toString() {
        String op=String.format("%20s ",this.make);
        op+=String.format("%20s ",this.model);
        op+=String.format("%5s ",this.year);
        return op;

    }
    

    /**
     * @return the make
     */
    public String getMake() {
        return make;
    }

    /**
     * @param make the make to set
     */
    public void setMake(String make) {
        this.make = make;
    }

    /**
     * @return the model
     */
    public String getModel() {
        return model;
    }

    /**
     * @param model the model to set
     */
    public void setModel(String model) {
        this.model = model;
    }

    /**
     * @return the year
     */
    public int getYear() {
        return year;
    }

    /**
     * @param year the year to set
     */
    public void setYear(int year) {
        this.year = year;
    }

    /**
     * @return the vin
     */
    public long getVin() {
        return vin;
    }

    /**
     * @param vin the vin to set
     */
    public void setVin(long vin) {
        this.vin = vin;
    }

}
