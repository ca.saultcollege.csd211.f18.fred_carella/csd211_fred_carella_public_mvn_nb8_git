/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ___fall2020.lecture4b.unit_tests;

import java.util.Scanner;

/**
 *
 * @author students
 */
public class App {

    private Car[] cars = new Car[10];
    private int currentIndex = 0;
    private Scanner input;

    public void run() {
        Car mustang = new Car("Ford", "Mustang", 2019, 123);
        System.out.println(mustang);
        Car corvette = new Car("Chevrolet", "Corvette", 2020, 456);
        System.out.println(corvette);

        System.out.println(corvette.equals(mustang));
        System.out.println(corvette.equals("mustang"));

        Car c3 = corvette;
        System.out.println(corvette.equals(c3));

        addCar(corvette);
        if (findCar(corvette) != null) {
            System.out.println("found");
        } else {
            System.out.println("not found...");
        }

        if (findCar(new Car("Chevrolet", "Corvette", 2020, 4568888)) != null) {
            System.out.println("found");
        } else {
            System.out.println("not found...");
        }

        if (findCar(new Car("Chevrolet", "Corvette", 2020, 456)) != null) {
            System.out.println("found");
        } else {
            System.out.println("not found...");
        }

        edit();

    }

    public void addCar(Car car) {
        cars[currentIndex++] = car;
    }

    // a car is in the array if its vin is the same as a car already in the array
    public Car findCar(Car car) {
        for (Car c : cars) {
            if (c == null) {
                return null;
            }
            if (c.equals(car)) {
                return c;
            }
        }
        return null;
    }

    private void edit() {
        input = new Scanner(System.in); // reset the scanner

        list(1);
        System.out.println("Which car would you like to edit ?:");
        int choice = input.nextInt();
        input = new Scanner(System.in); // reset the scanner
        if ((choice < currentIndex + 1) && choice > 0) {
            Car c = cars[choice - 1];
            System.out.println("Make: " + c.getMake());
            c.setMake(getInput(c.getMake()));
            System.out.println("Model: " + c.getModel());
            c.setModel(getInput(c.getModel()));
            System.out.println("Year: " + c.getYear());
            c.setYear(getInput(c.getYear()));
        } else {
            System.out.println("Choice out of bounds");
        }
        System.out.println("");
        
        list(1);
    }

    private String getInput(String s) {
        String ss = input.nextLine();
        if (ss.trim().isEmpty()) {
            return s;
        }
        Scanner in2 = new Scanner(ss);
        return in2.nextLine();
    }

    private int getInput(int i) {
        String s = input.nextLine();
        if (s.trim().isEmpty()) {
            return i;
        }
        Scanner in2 = new Scanner(s);
        return in2.nextInt();
    }

    private void list(int i) {
        for (int j = 0; j < cars.length; j++) {
            if (cars[j] == null) {
                return;
            }
            System.out.println((j + 1) + ". " + cars[j].toString());
        }

    }

}
