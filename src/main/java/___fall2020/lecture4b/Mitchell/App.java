/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ___fall2020.lecture4b.Mitchell;

/**
 *
 * @author mitch
 */
public class App {
    private Car[] cars = new Car[10];
    private Car[] cars2 = new Car[10];
    private int currentIndex = 0;
    
    public void run(){
        //creates cars
        Car car1 = new Car("Ford", "Mustang", 2019, 1234l);
        Car car2 = new Car("Ford", "Mustang", 2019, 12345l);
        Car car3 = new Car("Ford", "Mustang", 2019, 1234l);
        Car car4 = car1;
        //prints the cars information to the screen
        /*System.out.println(car1);
        System.out.println(car2);
        System.out.println(car3);
        System.out.println(car4);
        //if duplicate cars are in the system, this will let us know
        if(car2.equals(car1))
            System.out.println("car1 and car2 are the same");
        else
            System.out.println("car1 and car2 are not the same");
        if(car3.equals(car1))
            System.out.println("car1 and car3 are the same");
        else
            System.out.println("car1 and car3 are not the same");
        if(car4.equals(car1))
            System.out.println("car1 and car4 are the same");
        else
            System.out.println("car1 and car4 are not the same");
        if(car3.equals(car2))
            System.out.println("car2 and car3 are the same");
        else
            System.out.println("car2 and car3 are not the same");
        if(car4.equals(car2))
            System.out.println("car2 and car4 are the same");
        else
            System.out.println("car2 and car4 are not the same");
        if(car4.equals(car3))
            System.out.println("car3 and car4 are the same");
        else
            System.out.println("car3 and car4 are not the same");
        //displays how many people have been created
        System.out.println("There are " + countPeople() + " people created");
        //displays how many cars have been created
        System.out.println("There are " + countCars() + " car's created");*/
        
        addCar(car1);
        addCar(car2);
        addCar(car3);
        addCar(car4);
        listCar();
        deleteCar(car2);
        listCar();
        System.out.println(findCar(car2));
    }
    public int countPeople(){
            return Person.getCount();
        }
    public int countCars(){
        return Car.getCount();
    }
    
    public void addCar(Car car){
        cars[currentIndex++] = car;
    }
    
    public void deleteCar(Car car){
        for(int x = 0; x < currentIndex; x++){
            if(car == cars[x]){
                for(int i = 0; i < cars.length; i++){
                    if(i<x){
                        cars2[i] = cars[i];
                    }
                    else if(i>x){
                        cars2[i-1] = cars[i];
                    }
                }
                for(int i = 0; i < currentIndex; i++){
                    cars[i] = cars2[i];
                }
            }
        }
    }
    
    private void listCar(){
        for(int i = 0; i < cars.length; i++){
            if(cars[i] == null)
                break;
            System.out.println((i+1) + ".\n" + cars[i]);
        }
    }
    
    public Car findCar(Car car) {
        for (Car c : cars) {
            if (c == null) {
                return null;
            }
//            if (c.equals(car)) {
            if (car==c) {
                return c;
            }
        }
        return null;
    }
}
