/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ___fall2020.lecture4b.Mitchell;

/**
 *
 * @author mitch
 */
public class Person extends Object implements java.io.Serializable {
    // Attributes of a person
    // defined as instance variables
    private int age;
    private double height; // in cm
    private double weight; // in kg
    private String firstname;
    private String lastname;
    private char gender;
    // class variables
    private static int count;
    
    public Person(){
        count++;
    }
    
    public Person(String firstname, String lastname){
        this.firstname = firstname;
        this.lastname = lastname;
        count++;
    }
    
    public Person(String firstname, String lastname, int age, char gender){
        this.firstname = firstname;
        this.lastname = lastname;
        this.age = age;
        this.gender = gender;
        count++;
    }
    
    public Person(String firstname, String lastname, int age, double height, double weight, char gender){
        this.firstname = firstname;
        this.lastname = lastname;
        this.age = age;
        this.height = height;
        this.weight = weight;
        this.gender = gender;
        count++;
    }
    
    public double getAge(){
        return age;
    }
    public void setAge(int age) {
        this.age = age;
    }
    
    public double getHeight(){
        return height;
    }
    public void setHeight(double height){
        this.height = height;
    }
    
    public double getWeight(){
        return weight;
    }
    public void setWeight(double weight){
        this.weight = weight;
    }
    
    public String getFirstname(){
        return firstname;
    }
    public void setFirstname(String firstname){
        this.firstname = firstname;
    }
    
    public String getLastname(){
        return lastname;
    }
    public void setLastname(String lastname){
        this.lastname = lastname;
    }
    
    public char getGender(){
        return gender;
    }
    public void setGender(char gender){
        this.gender = gender;
    }
    
    @Override
    public String toString(){
        return "Name : " + getFirstname() + " " + getLastname() +
                "\nAge : " + getAge() + 
                "\nHeight : " + getHeight() + "cm" +
                "\nWeight : " + getWeight() + "kg" +
                "\nGender : " + getGender() + "\n";
    }
    
    @Override
    public boolean equals(Object o){
        if(!(o instanceof Person))
            return false;
        if(this == o)
            return true;
        return false;
    }

    /**
     * @return the count
     */
    public static int getCount() {
        return count;
    }

    /**
     * @param aCount the count to set
     */
    public static void setCount(int aCount) {
        count = aCount;
    }
}
