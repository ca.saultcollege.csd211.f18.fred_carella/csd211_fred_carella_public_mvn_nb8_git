/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ___fall2020.lecture4b.Mitchell;

/**
 *
 * @author mitch
 */
public class Car extends Object implements java.io.Serializable {
    // instance variables
    private String make;
    private String model;
    private int year;
    private Person owner;
    private long vin;
    // class variables
    private static int count;

    public Car(){
        count++;
    }
    
    public Car(String make, String model, int year){
        this.make = make;
        this.model = model;
        this.year = year;
        count++;
    }
    
    public Car(String make, String model, int year, Person owner){
        this.make = make;
        this.model = model;
        this.year = year;
        this.owner = owner;
        count++;
    }
    
    public Car(String make, String model, int year, long vin){
        this.make = make;
        this.model = model;
        this.year = year;
        this.vin = vin;
        count++;
    }
    
    public Car(String make, String model, int year, long vin, Person owner){
        this.make = make;
        this.model = model;
        this.year = year;
        this.vin = vin;
        this.owner = owner;
        count++;
    }
    
    /**
     * @return the make
     */
    public String getMake() {
        return make;
    }

    /**
     * @param make the make to set
     */
    public void setMake(String make) {
        this.make = make;
    }

    /**
     * @return the model
     */
    public String getModel() {
        return model;
    }

    /**
     * @param model the model to set
     */
    public void setModel(String model) {
        this.model = model;
    }

    /**
     * @return the year
     */
    public int getYear() {
        return year;
    }

    /**
     * @param year the year to set
     */
    public void setYear(int year) {
        this.year = year;
    }

    /**
     * @return the vin
     */
    public long getVin() {
        return vin;
    }

    /**
     * @param vin the vin to set
     */
    public void setVin(long vin) {
        this.vin = vin;
    }
    
    @Override
    public String toString(){
        String s="";
        s+= "Make : " + getMake() +
            "\nModel : " + getModel() +
            "\nYear : " + getYear() +
            "\nVIN : " + getVin() + "\n";
        Person owner = getOwner();
        if(owner!=null)
            s+=owner.toString();
        return s;
    }

    @Override
    public boolean equals(Object o){
        if(!(o instanceof Car))
            return false;
        if(this == o)
            return true;
        return getVin() == ((Car)o).getVin();
    }
    
    /**
     * @return the count
     */
    public static int getCount() {
        return count;
    }

    /**
     * @param aCount the count to set
     */
    public static void setCount(int aCount) {
        count = aCount;
    }

    /**
     * @return the owner
     */
    public Person getOwner() {
        return owner;
    }

    /**
     * @param owner the owner to set
     */
    public void setOwner(Person owner) {
        this.owner = owner;
    }
}
