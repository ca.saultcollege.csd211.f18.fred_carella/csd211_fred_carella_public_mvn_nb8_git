/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ___fall2020.lecture4b.Mitchell;

/**
 *
 * @author mitch
 * Date: 2020-11-16
 * Description: Your app will implement a CRUD (Create, Update, Delete) 
 * menu for adding, editing and deleting cars from the used car lot.  
 */
public class Main {
    public static void main(String[] args) {
        new App().run();
    }
}
