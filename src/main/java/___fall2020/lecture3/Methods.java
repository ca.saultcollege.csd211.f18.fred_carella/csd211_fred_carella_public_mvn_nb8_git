/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ___fall2020.lecture3;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author fred
 */
public class Methods {
    public static void main(String[] args) {
        double x=5.3;
        double y=10.6;
        
        // x and y are called arguments
        double a1=add(x,y);
        double a2=subtract(x,y);
        double a3=multiply(x,y);
        double a4 = 0;
        try {
            a4=divide(x,y);
        } catch (Exception ex) {
            Logger.getLogger(Methods.class.getName()).log(Level.SEVERE, null, ex);
            System.exit(0);
        }
        
        System.out.println("a1="+a1);
        System.out.println("a2="+a2);
        System.out.println("a3="+a3);
        System.out.println("a4="+a4);
        
        System.out.println("12.2 * 3.1 =" + multiply(12.2, 3.1));
    }

    // x and y are parameters
    private static double add(double x, double y) {
        return x+y;
    }

    private static double subtract(double a, double b) {
        return a-b;
    }

    private static double multiply(double x, double y) {
        return x*y;
    }

    private static double divide(double x, double y) throws Exception {
        if(y==0)
            throw new Exception("Cannot divide by zero");
        return x/y;
    }

}
