/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ___fall2020.lecture3.ex5_menu_loop;

import java.util.Scanner;

/**
 *
 * @author students
 */
public class App {
    private Scanner input=new Scanner(System.in);
    public void run(){
        System.out.println("Running App...");
        boolean done=false;
        while(!done){
            input=new Scanner(System.in);
            System.out.println("1. Option 1");
            System.out.println("2. Option 2");
            System.out.println("3. Option 3");
            System.out.println("99. Exit");
            System.out.println("Enter an option");
            
            int choice=0;
            try{
                choice=input.nextInt();
            }catch(Exception e){
                System.out.println("Problem, try again...");
                continue;
            }        
            switch(choice){
                case 1: doOption1();
                        break;
                case 2: doOption2();
                        break;
                case 3: doOption3();
                        break;
                case 99: done=true;
                        break;
                default: System.out.println("Bad input, try again please.");
            }
            
            
        }
        
    }

    private void doOption1() {
        System.out.println("in option 1");
    }

    private void doOption2() {
        System.out.println("in option 2");
    }

    private void doOption3() {
        System.out.println("in option 3");
    }
}
