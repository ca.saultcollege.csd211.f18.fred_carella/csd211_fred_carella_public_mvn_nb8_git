/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ___fall2020.lecture3.ex4;

/**
 *
 * @author fcarella
 */
public class App {

    public void run() {
        Parent parent=new Parent();
        Child child=new Child();
        
        System.out.println("is parent instanceOf Parent ==> "+ (parent instanceof Parent) );
        System.out.println("is child instanceOf Child ==> "+ (child instanceof Child) );
        System.out.println("is parent instanceOf Child ==> "+ (parent instanceof Child) );
        System.out.println("is child instanceOf Parent ==> "+ (child instanceof Parent) );
        System.out.println("is parent instanceOf MyInterface ==> "+ (parent instanceof MyInterface) );
    }

}
