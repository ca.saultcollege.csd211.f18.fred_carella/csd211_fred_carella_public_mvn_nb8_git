/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ___fall2020.lecture3.A_F20_examples.ex3_loops;

/**
 *
 * @author students
 */
public class App {
    public void run(){
        System.out.println("App is running...");
        // reference Operators01.java
        
        // Loops
        // for loops
        int index=0;
        
        for(index=0; index < 10; index++){
            System.out.println("index="+ index);
        }
        
        for(int j=0;j<10;j++){
            System.out.println("j="+j);
            if(j==4)
                break;
        }
        System.out.println("==============================");
        
        for(int j=0;j<10;j++){
            if(j<4)
                continue;
            System.out.println("j="+j);
        }
        
        
        String[] names=new String[3];
        // populating the array with a for loop
        for(int j=0;j<3;j++){
            names[j]=new String("Fred "+j);
        }
        
        // iterating through an array with a for loop
        // using an index
        for(int j=0;j<3;j++){
            System.out.println("names["+j+"]="+names[j]);
        }
        
        // iterating through an array with a for each loops
        int i=0;
        for(String name : names){
            System.out.println("Name["+i+"]="+name);
            i++;
        }
        
        // while loops
        boolean accelerate_switch=true;
        int speed=0;
        int stop_count=10;
        
        index=0;
        while(accelerate_switch==true){
            accelerate(speed++);
            index++;
            if(index>=stop_count)
                break;
        }
        
//        while(true){
//            
//            // endless loop
//            System.out.println("Endless loop...");
//        }
        
        index=0;
        // its possible for a while block to not execute at all
        while(index>0){
            System.out.println("index>0");
        }
        index=0;
        accelerate_switch=true;
        do{
            // accelerate motor
            System.out.println("Accelerating the motor");
            if(index>=10)
                accelerate_switch=false;
            index++;
        }while(accelerate_switch);
        
        // do while loops
    }

    private void accelerate(int i) {
        System.out.println("Accelerating "+i);
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
