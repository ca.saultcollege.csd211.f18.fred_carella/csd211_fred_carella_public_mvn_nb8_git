/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ___fall2020.lecture3.A_F20_examples.ex1;

import java.util.Scanner;

/**
 *
 * @author students
 */
public class Ex1 {
//    private static int anInt;

    public static void main(String[] args) {
        int x = 3;
        int y = 5;
        int z;

//        x=anInt;
        z = x + y;
        z = x - y;
        z = x * y;
        z = x / y;// z=0;
        z = x % y;// z=3

        int xMod = x % 2;// 1 if odd
        int yMod = y % 2;// 0 if even

        int num = 1234;// how do I get the 4.  Or how can I get the 
        // individual numbers 1 2 3 4
        int lsd0 = num % 10;// lsd=4
        int tmp = num / 10;// get rid of the 4
        int lsd1 = tmp % 10; // lsd1=3
        tmp /= 10;
        int lsd2 = tmp % 10; // lsd2=2

        String numS = "10";
        int n = Integer.parseInt(numS);
        System.out.println("n=" + n);

        // +
        z = x + y; // mathematical addition
        String firstName = "Fred";
        String lastName = "Carella";
        String fullName = firstName + lastName;// add 2 strings together
        // or the proper thing to say is we "concatenate" or "append"
        // 2 strings together

        x = 0;
        System.out.println("x=" + x); // x=0
        x++;// increment x. 
        System.out.println("x=" + x); // x=1

        System.out.println("x=" + (x++)); // x=1
        System.out.println("x=" + x); // x=2

        System.out.println("x=" + (++x)); // x=3
        System.out.println("x=" + x); // x=3

        // these 2 statements do the same thing, just different notation
        x = x + 2;
        x += 2; // this is "syntactic sugar"

        x -= 6;
        x /= 2;
        x *= 4;
        x %= 7;

        int speed_switch = 0;// 0 == off, 1 == slow, 2 == fast

        if (speed_switch == 0) {
            motor_speed(0);// set the speed to slow
        }
        if (speed_switch == 1) {
            motor_speed(1);
        }
        if (speed_switch == 2) {
            motor_speed(2);
        }

        if (speed_switch == 0) {
//            turn_the_light_on();
            motor_speed(0);

            if (speed_switch == 0) {
                // do this if true
            } else {
                // otherwise do this
                // turn_light_off();
            }
            if (speed_switch != 0) {
                // do this if its NOT true
            } else {
                // otherwise do this
                // turn_light_off();
            }

            x = 4;
            if (x >= 4) {
                System.out.println("x is greater than or equal to 4");
            } else {// not true or equal
                System.out.println("x is less than 4");
            }

            Scanner in = new Scanner(System.in);

            // Simple calculator menu
            System.out.println("1. Add 2 numbers");
            System.out.println("2. Subtract 2 numbers");
            System.out.println("3. Multiply 2 numbers");
            System.out.println("4. Divide 2 numbers");
            System.out.println("Please enter the operation to perform (1, 2, 3 or 4) : ");
            int choice = in.nextInt();

            if (choice == 1) {
                doAdd();
            } else if (choice == 2) {
                doSubtract();
            } else if (choice == 3) {
                doMult();
            } else {
                System.out.println("Wrong choice");
            }

            switch (choice) {
                case 1:
                    doAdd();
                    break;
                case 2:
                    doSubtract();
                    break;
                case 3:
                    doMult();
                    break;
                default:
                    System.out.println("Wrong Choice!");
            }
            
            x=1;
            y=2;
            
            if( (x==1) && (y==2) ){ // logical AND
                // if x=1 and y=2 do this
                System.out.println("true");
            }else{
                System.out.println("false");
            }
            
            if( (x==1) || (y==2) ){ // logical OR
                // if x=1 OR y=2 do this
                System.out.println("true");
            }else{
                System.out.println("false");
            }
            
            if( (x==1 || (x==4 && y<4) && (z>y))){
                System.out.println("true");
            }
            
            boolean b;
            if(x==1){
                System.out.println("do this");
                b=true;
            }else{
                System.out.println("do that");
                b=false;
            }
            b=(x==1)?true:false;
            
            int count = 1;
            System.out.println("I flipped the switch "+count+ (count==1?"time":"times"));

        }

    }

    private static void motor_speed(int motor_speed) {
        // method stub
        System.out.println("motor_speed method");
    }

    private static void doAdd() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private static void doSubtract() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private static void doMult() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
