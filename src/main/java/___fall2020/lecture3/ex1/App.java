/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ___fall2020.lecture3.ex1;

/**
 *
 * @author fcarella
 */
public class App {

    public void run() {
        int x = 10;
        int y = 3;
        int menuOption = 0;
        int doOption = 0;

        double z = (double) x / y;

        System.out.println("Result=" + z);

        System.out.println("x++ == " + x++);
        System.out.println("++x == " + ++x);

        if (x == 12) {
            System.out.println("x==12");
        }

        x = 1;
        y = 2;
        z = 4;

        if (x == 1) {
            y = 3;
            z = 5;
        }

        if (x == 1) {
            y = 3;
            z = 5;
        }

    }
}
