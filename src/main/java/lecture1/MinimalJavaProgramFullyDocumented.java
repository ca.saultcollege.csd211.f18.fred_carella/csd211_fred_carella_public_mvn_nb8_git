/*
 * A Minimal Java Program
 * Always place comments at the top decribing what the application does.
 * 
 * Author :         Fred Carella
 * Date:            the date
 * Description :    Lecture 1 on a minimal java program.  This program is
 *                  used as a basis to describe and understand the structure of 
 *                  a java program.
 * 
        NOTE** place all the exercises in a packages called lectureN.exercises
        NOTE** hand in yur code via git
        NOTE** place comments indicating the INPUT, PROCESS and OUTPUT 
               sections of your program.
 */



/*
 * The first line is a package statement which tells the system what package this 
 * class belongs to.  A package contains related classes and is represented in
 * the filesystem as a folder on disk.  
 * 
 */

package lecture1;

/**
 * The @author directive is a javadoc directive indicating the author of the system.  
 * javadoc is a utility for producing self documenting code.  Running the javadoc utility 
 * will create html documentation for your program.
 * 
 * javadoc documentation : {@link http://www.oracle.com/technetwork/java/javase/documentation/index-137868.html}
 * 
 * @author fcarella
 */

/*
 * the class keyword:
 * 
 * Every java program is a class.  Therefore we always begin by defining the 
 * class for our program.
 * 
 * A class must have the same name as the file its defined in.
 * 
 * 
 * 
 */
public class MinimalJavaProgramFullyDocumented {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
    }
}
