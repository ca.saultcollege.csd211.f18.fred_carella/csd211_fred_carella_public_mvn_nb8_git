Lecture 1

the minimal java program
discuss the structure of a java program
every program is a "class" and has the following signature

    public class NameOfTheClass {
    }

in order for a java class to execute it must have an entry point, the place 
  that it goes to when you first run it.
a programs entry point is called "main" and it must have the following 
  signature (ie it must look like this)

    public static void main(String[] args) {
        // your program starts here

    }
a complete minimal program looks like this

package lecture1;
public class MinimalJavaProgram_1 {
    public static void main(String[] args) {
    }
}

the class must be in a file with the same name of the class so in our example,
  the file name is NameOfTheClass.java.
Object
    an Object is an instantiation of a class.
    discuss references, classes, objects, null reference.
you can't do anything with a class, its just a "blueprint" for a real object.
a class must be instantiated in order for it to be useful.
TEACHER : go over MinimalJavaProgramFullyDocumented_1.java and MinimalJavaProgramFullyDocumented_2.java
TEACHER : go over 
    how to create a class from scratch in Netbeans.
    compile it
    run it
    show some errors.
    print HelloWorld.
STUDENT IN CLASS EXERCISE : 




STUDENT IN CLASS EXERCISE :
    create your own minimal java program based on MinimalJavaProgramFullyDocumented_1.java.
      Call it <FirstnameLastname>Minimal.java, of course
      use your own first an last name.
    make your program output "Hello, my name is Firstname Lastname!"
    run it.
    What is the output (should be nothing.)