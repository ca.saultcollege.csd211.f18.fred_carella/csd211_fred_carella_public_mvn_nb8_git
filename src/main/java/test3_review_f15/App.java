/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test3_review_f15;

/**
 *
 * @author fcarella
 */
public class App {

    public void run() {
        System.out.println("Test 3 Review");
        
        Animal moose=new Animal();
        moose.setNumLegs(4);
        moose.setSpecies("Alces");
        moose.setTagged(false);
        moose.setWeight(1000);

        Animal deer=new Animal(4, 200, true, "Alces");
        
        if(deer.equals(moose)){
            System.out.println("moose == deer");
        }else
            System.out.println("moose != deer");
        
        Animal cheetah=new Animal(4, 150, false, "A Jubatus");
        if(cheetah.equals(moose)){
            System.out.println("moose == cheetah");
        }else
            System.out.println("moose != cheetah");
        
        cheetah=deer;
        if(cheetah.equals(moose)){
            System.out.println("moose == cheetah");
        }else
            System.out.println("moose != cheetah");
        
        
    }
}
