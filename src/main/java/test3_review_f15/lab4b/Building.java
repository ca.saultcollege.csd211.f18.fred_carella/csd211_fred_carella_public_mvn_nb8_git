/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test3_review_f15.lab4b;

/**
 *
 * @author fcarella
 */
public class Building {
    private int numFloors;
    private int numRooms;
    private String address;
    private char plan;
    private boolean condemned;

    @Override
    public String toString() {
        return super.toString(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj); //To change body of generated methods, choose Tools | Templates.
    }

    
    public Building() {
        this.numFloors = 0;
        this.numRooms = 0;
        this.address = "Not Set";
        this.plan = '_';
        this.condemned = false;
    }

    public Building(int numFloors) {
        this.numFloors = numFloors;
        this.numRooms = 0;
        this.address = "Not Set";
        this.plan = '_';
        this.condemned = false;
    }
    public Building(int numFloors, int numRooms) {
        this.numFloors = numFloors;
        this.numRooms = numRooms;
        this.address = "Not Set";
        this.plan = '_';
        this.condemned = false;
    }
    public Building(int numFloors, int numRooms, String address) {
        this.numFloors = numFloors;
        this.numRooms = numRooms;
        this.address = address;
        this.plan = '_';
        this.condemned = false;
    }
    public Building(int numFloors, int numRooms, String address, char plan) {
        this.numFloors = numFloors;
        this.numRooms = numRooms;
        this.address = address;
        this.plan = plan;
        this.condemned = false;
    }
    public Building(int numFloors, int numRooms, String address, char plan, boolean condemned) {
        this.numFloors = numFloors;
        this.numRooms = numRooms;
        this.address = address;
        this.plan = plan;
        this.condemned = condemned;
    }
    
    
    /**
     * @return the numFloors
     */
    public int getNumFloors() {
        return numFloors;
    }

    /**
     * @param numFloors the numFloors to set
     */
    public void setNumFloors(int numFloors) {
        this.numFloors = numFloors;
    }

    /**
     * @return the numRooms
     */
    public int getNumRooms() {
        return numRooms;
    }

    /**
     * @param numRooms the numRooms to set
     */
    public void setNumRooms(int numRooms) {
        this.numRooms = numRooms;
    }

    /**
     * @return the address
     */
    public String getAddress() {
        return address;
    }

    /**
     * @param address the address to set
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * @return the plan
     */
    public char getPlan() {
        return plan;
    }

    /**
     * @param plan the plan to set
     */
    public void setPlan(char plan) {
        this.plan = plan;
    }

    /**
     * @return the condemned
     */
    public boolean isCondemned() {
        return condemned;
    }

    /**
     * @param condemned the condemned to set
     */
    public void setCondemned(boolean condemned) {
        this.condemned = condemned;
    }
    
    
}
