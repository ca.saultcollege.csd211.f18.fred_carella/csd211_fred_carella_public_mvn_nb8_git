/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test3_review_f15.lab4b;

import java.util.Scanner;

/**
 *
 * @author fcarella
 */
public class App {

    private static Building[] buildings = new Building[100];
    private static int currentBuilding = 0;

    public void run() {
        System.out.println("Building App");

        Building b = new Building();
        Building b2 = new Building(10);
        if (b.equals(b2)) {
            System.out.println("They are the same building");
        }

        String menu = ""
                + "\n\n1. Add a building\n"
                + "2. Sell a building\n"
                + "3. List all buildings\n"
                + "4. Delete a building\n"
                + "5. Edit a building\n"
                + "6. Quit";

        boolean quit = false;
        Scanner input = new Scanner(System.in);

        while (!quit) {
            System.out.println(menu);
            int option = input.nextInt();

            switch (option) {
                case 1:
                    try {
                        addABuilding();
                    } catch (Exception ex) {
                        System.out.println(ex.getMessage());
                    }
                    break;
                case 2:
                    listAllBuildings();
                    break;
                case 3:
                    deleteABuilding();
                    break;
                case 4:
                    quit = true;
                    break;
                default:
                    System.out.println("Invalid entry, try again...");
            }

        }
    }

    private void addABuilding() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void sellABuilding() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void listAllBuildings() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void deleteABuilding() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void editBuilding() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
