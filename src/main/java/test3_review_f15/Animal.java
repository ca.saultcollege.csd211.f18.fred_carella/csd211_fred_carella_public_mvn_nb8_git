/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test3_review_f15;

/**
 *
 * @author fcarella
 */
public class Animal {
    private int numLegs;
    private double weight;
    private boolean tagged;
    private String species;
    
    
    public String toString(){
        return species;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj==this)
            return true;
        if( !(obj instanceof Animal))
            return false;
        
        Animal a= (Animal)obj;
        
        if(getSpecies().equals(a.getSpecies()))
            return true;
        
        
        return false;
    }
    
    public Animal(){
        this.numLegs = 0;
        this.weight = 0;
        this.tagged = false;
        this.species = "not set";
    }

    public Animal(int numLegs, double weight, boolean tagged, String species) {
        this.numLegs = numLegs;
        this.weight = weight;
        this.tagged = tagged;
        this.species = species;
    }
    public Animal(int numLegs, double weight){
        this.numLegs = numLegs;
        this.weight = weight;
        this.tagged = false;
        this.species = "not set";
    }
    public Animal(int numLegs) {
        this.numLegs = numLegs;
        this.weight = 0;
        this.tagged = false;
        this.species = "not set";
    }

    /**
     * @return the numLegs
     */
    public int getNumLegs() {
        return numLegs;
    }

    /**
     * @param numLegs the numLegs to set
     */
    public void setNumLegs(int numLegs) {
        this.numLegs = numLegs;
    }

    /**
     * @return the weight
     */
    public double getWeight() {
        return weight;
    }

    /**
     * @param weight the weight to set
     */
    public void setWeight(double weight) {
        this.weight = weight;
    }

    /**
     * @return the tagged
     */
    public boolean isTagged() {
        return tagged;
    }

    /**
     * @param tagged the tagged to set
     */
    public void setTagged(boolean tagged) {
        this.tagged = tagged;
    }

    /**
     * @return the species
     */
    public String getSpecies() {
        return species;
    }

    /**
     * @param species the species to set
     */
    public void setSpecies(String species) {
        this.species = species;
    }
}
