/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package examples.music;

import java.util.ArrayList;

/**
 *
 * @author fcarella
 */
public class Instrument {
    private double volume;// percentage
    private Tab tab;
    private ArrayList storage;
    private int[] arrayStore;
    private String name;

    public Instrument() {
        volume=0;
        tab=null;
        storage=new ArrayList();
        arrayStore=new int[10];
        name="instrument!";
    }


    public ArrayList getStorage(){
        return storage;
    }

    public int[] getArrayStore(){
        return arrayStore;
    }
    
    public double getVolume(){
        return volume;
    }
    public void setVolume(double volume){
        this.volume=volume;
    }

    public Tab getTab(){
        return tab;
    }

    public void setTab(Tab tab){
        this.tab=tab;
    }

    /**
     * @param storage the storage to set
     */
    public void setStorage(ArrayList storage) {
        this.storage = storage;
    }

    /**
     * @param arrayStore the arrayStore to set
     */
    public void setArrayStore(int[] arrayStore) {
        this.setArrayStore(arrayStore);
    }


    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    public void play(){
        System.out.println("noise!!");
    }

    @Override
    public String toString() {
        return getName();
    }

}
