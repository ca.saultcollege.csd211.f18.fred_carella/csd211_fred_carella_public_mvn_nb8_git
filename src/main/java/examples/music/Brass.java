/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package examples.music;

/**
 *
 * @author fcarella
 */
public class Brass extends Instrument {

    public Brass() {
        setName("Brass Instrument");
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    @Override
    public String toString() {
        return getName();
    }


}
