/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package examples.music;

/**
 *
 * @author fcarella
 */
public class Wind extends Instrument {

    public Wind() {
        setName("Generic Wind Instrument");
    }
    @Override
    public String toString() {
        return getName();
    }

    @Override
    public void play(){
        System.out.println("Whoosh...");
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof Wind){
            Wind w=(Wind)obj;
            if(this.getName().equals(w.getName())){
                return true;
            }
        }
        return false;
    }



}
