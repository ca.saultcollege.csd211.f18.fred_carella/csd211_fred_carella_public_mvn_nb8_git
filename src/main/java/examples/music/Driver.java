/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package examples.music;

/**
 *
 * @author fcarella
 */
public class Driver {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Instrument a[]=new Instrument[4];
        Instrument i=new Instrument();
        Wind w=new Wind();
        Brass b=new Brass();
        Trumpet t=new Trumpet();
        a[0]=i;
        a[1]=w;
        a[2]=b;
        a[3]=t;

        for(Instrument gilles:a){
            System.out.println("I am a "+gilles);
            gilles.play();
        }


    }

}
