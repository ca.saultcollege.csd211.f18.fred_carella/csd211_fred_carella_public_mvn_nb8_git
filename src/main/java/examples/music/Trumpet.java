/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package examples.music;

/**
 *
 * @author fcarella
 */
public class Trumpet extends Brass {

    public Trumpet() {
        setName("Trumpet");
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    @Override
    public String toString() {
        return getName();
    }

    @Override
    public void play() {
        System.out.println("trumpet sound...");
    }



}
