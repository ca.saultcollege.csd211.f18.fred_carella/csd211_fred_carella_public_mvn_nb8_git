/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package examples.music;

/**
 *
 * @author fcarella
 */
public class BagPipe extends Wind {

    public BagPipe() {
        setName("Bag Pipe");
    }

    @Override
    public void play() {
        System.out.println("Bagpipe sound....");
    }

    @Override
    public String toString() {
        return getName();
    }


}
