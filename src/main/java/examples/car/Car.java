package examples.car;

public class Car extends Object {
	private int year;
	private String make;
	private String model;
	public Car(){
		
	}
	public Car(String make, String model, int year){
		this.year=year;
		this.model=model;
		this.make=make;
	}
	
	@Override
	public String toString() {
		return "Car [year=" + year + ", make=" + make + ", model=" + model
				+ "]";
	}

	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public String getMake() {
		return make;
	}
	public void setMake(String make) {
		this.make = make;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}

}
