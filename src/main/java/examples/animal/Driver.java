/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package examples.animal;

/**
 *
 * @author fcarella
 */
public class Driver {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Animal animals[]=new Animal[3];
        Animal a=new Animal();
        Dog d=new Dog();
        Cat c=new Cat();

        animals[0]=a;
        animals[1]=d;
        animals[2]=c;

        for(Animal aa:animals){
            System.out.println("Hello, I'm a "+aa);
            aa.talk();
        }

    }

}
