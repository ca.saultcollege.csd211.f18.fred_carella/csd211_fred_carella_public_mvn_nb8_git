/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package examples.animal;

/**
 *
 * @author fcarella
 */
public class Cat extends Animal {

    public Cat() {
        setName("Cat");
    }

    @Override
    public void talk(){
        System.out.println("Meow...");
    }

    @Override
    public String toString() {
        return getName();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Cat) {
            Cat a = (Cat) obj;
            if (getName().equals(a.getName())) {
                return true;
            }
        }
        return false;
    }

}
