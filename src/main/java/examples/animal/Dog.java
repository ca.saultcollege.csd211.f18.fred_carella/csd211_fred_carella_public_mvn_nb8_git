/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package examples.animal;

/**
 *
 * @author fcarella
 */
public class Dog extends Animal {

    public Dog() {
        setName("Dog");
    }

    @Override
    public void talk(){
        System.out.println("Woof...");
    }

    @Override
    public String toString() {
        return getName();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Dog) {
            Dog a = (Dog) obj;
            if (getName().equals(a.getName())) {
                return true;
            }
        }
        return false;
    }

}
