/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package examples.animal;

/**
 *
 * @author fcarella
 */
public class Animal {

    private String name;

    public Animal() {
        this.name = "I'm an animal";
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Animal) {
            Animal a = (Animal) obj;
            if (name.equals(a.getName())) {
                return true;
            }
        }
        return false;
    }

    @Override
    public String toString() {
        return name;
    }

    public void talk(){
        System.out.println("arg...");
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }
}
