/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package _fall2018_current.lecture4.exercises;

import java.util.Scanner;

/**
 *
 * @author fcarella
 */
public class App {
    private Scanner input=new Scanner(System.in);

    public App() {
    }
    public void run(){
        boolean done=false;
        int choice=0;
        while(!done){
            System.out.println("1. Exercise 1");
            System.out.println("2. Exercise 2");
            System.out.println("99. Exit");
            choice=input.nextInt();
            switch(choice){
                case 1: exercise1();
                    break;
                case 2: exercise2();
                    break;
                case 99: System.out.println("Good bye");
                    System.exit(0);
                    break;
                default: System.out.println("Sorry try again");
                    break;
            }
        }
    }
    private void exercise1(){
        System.out.println("Exercise 1");
        Person joe=new Person();
        joe.setFirstname("Joe");
        joe.setLastname("Walsh");
        joe.setAge(28);
        
        Person steve=new Person("Steve", "Logan", 30);
        Person fred=new Person("Fred", "Carella");
        System.out.println(joe);
        System.out.println(steve);
        System.out.println(fred);
    }
    private void exercise2(){
        System.out.println("Subtract");
    }}
