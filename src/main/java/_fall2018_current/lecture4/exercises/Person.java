/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package _fall2018_current.lecture4.exercises;

/**
 *
 * @author fcarella
 */
public class Person {
    private String firstname;
    private String lastname;
    private int age;

    // no argument constructor
    public Person(){
        
    }

    public Person(String firstname, String lastname, int age) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.age = age;
    }
    
    public Person(String firstname, String lastname) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.age = 50;
    }

    public String toString() {
//        return super.toString(); //To change body of generated methods, choose Tools | Templates.
        return "Details:\n"+
                firstname+ " "+
                lastname+" "+
                age;
    }
    
    
    /**
     * @return the firstname
     */
    public String getFirstname() {
        return firstname;
    }

    /**
     * @param firstname the firstname to set
     */
    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    /**
     * @return the lastname
     */
    public String getLastname() {
        return lastname;
    }

    /**
     * @param lastname the lastname to set
     */
    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    /**
     * @return the age
     */
    public int getAge() {
        return age;
    }

    /**
     * @param age the age to set
     */
    public void setAge(int age) {
        this.age = age;
    }
    
 
}
