/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package _fall2018_current.lecture4.person;

/**
 *
 * @author fcarella
 */
public class Main2 {
    public static void main(String[] args) {
        Person2 p1=new Person2("Joe", "Maxwell", "123");
        Person2 p2=new Person2("Noel", "Hall", "123");
        
        if(p1.equals(p2))
            System.out.println("Equal");
        else
            System.out.println("Not Equal");
    }
}
