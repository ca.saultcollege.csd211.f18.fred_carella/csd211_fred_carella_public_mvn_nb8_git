/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package _fall2018_current.lecture4.person;

import lecture4.person.*;

/**
 *
 * @author fcarella
 */
public class Person {
    // Attributes of a person
    // defined as instance variables
    private int age; //
    private int height; // in cm
    private int weight ;//  in kg
    private String firstname;
    private String lastname;
    private char gender;

    /**
     * @return the age
     */
    public int getAge() {
        return age;
    }

    /**
     * @param age the age to set
     */
    public void setAge(int age) {
        this.age = age;
    }

    /**
     * @return the height
     */
    public int getHeight() {
        return height;
    }

    /**
     * @param height the height to set
     */
    public void setHeight(int height) {
        this.height = height;
    }

    /**
     * @return the weight
     */
    public int getWeight() {
        return weight;
    }

    /**
     * @param weight the weight to set
     */
    public void setWeight(int weight) {
        this.weight = weight;
    }

    /**
     * @return the firstname
     */
    public String getFirstname() {
        return firstname;
    }

    /**
     * @param firstname the firstname to set
     */
    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    /**
     * @return the lastname
     */
    public String getLastname() {
        return lastname;
    }

    /**
     * @param lastname the lastname to set
     */
    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    /**
     * @return the gender
     */
    public char getGender() {
        return gender;
    }

    /**
     * @param gender the gender to set
     */
    public void setGender(char gender) {
        this.gender = gender;
    }

    public Person() {
    }
    public Person(String fname, String lname) {
        firstname=fname;
        lastname=lname;
    }

    @Override
    public String toString() {
        String s="";
        s+="Name :"+" "+firstname+" "+lastname;
        s+="Age : "+age;
        s+="Height : "+height+" cm";
        s+="Weight :"+weight+"kg";
        s+="Gender : "+gender;
        return s;
    }

    @Override
    public boolean equals(Object obj) {
        // define equality as having the same first and last name.
        Person other=(Person)obj;
        if(this.firstname.equals(other.getFirstname())){
            if(this.lastname.equals(other.getLastname())){
                return true;
            }
        }
        return false;
    }

}
