/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package _fall2018_current.lecture4.person;

/**
 *
 * @author fcarella
 */
public class Person2 {

    // Attributes of a person
    // defined as instance variables
    private int age; //
    private int height; // in cm
    private int weight;//  in kg
    private String firstname;
    private String lastname;
    private char gender;
    private String sin;

    public Person2() {
        this.firstname = "No firstname set";
        this.lastname = "No lastname set";
    }

    public Person2(String firstname, String lastname) {
        this.firstname = firstname;
        this.lastname = lastname;
    }
    public Person2(String firstname, String lastname, String sin) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.sin=sin;
    }

    public Person2(String firstname, String lastname, int age) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.age = age;

    }
    public Person2(String firstname, String lastname, int age, String sin) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.age = age;
        this.sin=sin;
    }

    public String toString() {
        return "Firstname=" + getFirstname() + " "
                + "Lastname=" + getLastname() + " "
                + "Age=" + getAge();
    }

    @Override
    public boolean equals(Object obj) {
        Person2 p = (Person2) obj;
        if (sin.equals(p.getSin())) {
            return true;
        }
        return false;

    }

    /**
     * @return the age
     */
    public int getAge() {
        return age;
    }

    /**
     * @param age the age to set
     */
    public void setAge(int age) {
        this.age = age;
    }

    /**
     * @return the height
     */
    public int getHeight() {
        return height;
    }

    /**
     * @param height the height to set
     */
    public void setHeight(int height) {
        this.height = height;
    }

    /**
     * @return the weight
     */
    public int getWeight() {
        return weight;
    }

    /**
     * @param weight the weight to set
     */
    public void setWeight(int weight) {
        this.weight = weight;
    }

    /**
     * @return the firstname
     */
    public String getFirstname() {
        return firstname;
    }

    /**
     * @param firstname the firstname to set
     */
    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    /**
     * @return the lastname
     */
    public String getLastname() {
        return lastname;
    }

    /**
     * @param lastname the lastname to set
     */
    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    /**
     * @return the gender
     */
    public char getGender() {
        return gender;
    }

    /**
     * @param gender the gender to set
     */
    public void setGender(char gender) {
        this.gender = gender;
    }

    /**
     * @return the sin
     */
    public String getSin() {
        return sin;
    }

    /**
     * @param sin the sin to set
     */
    public void setSin(String sin) {
        this.sin = sin;
    }

}
