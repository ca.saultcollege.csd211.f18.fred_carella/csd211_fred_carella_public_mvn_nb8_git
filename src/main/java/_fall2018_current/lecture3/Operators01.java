/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package _fall2018_current.lecture3;

import java.util.Scanner;
//

/**
 *
 * @author fcarella
 */
public class Operators01 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        {        // pre and postfix
            int x = 0;
            int y = 0;
            System.out.println(x++);// 0
            System.out.println(++x);// 2
            int this_is_a_really_long_name = 10;
            if (x == 5) {
                x = 6;
            }
            // arithmetic operators
            // + - * / %
            x = 5;
            y = 7;
            System.out.println(x + y);// 12
            System.out.println(x - y);// -2
            System.out.println(x * y);// 35
            System.out.println(x / y);// 0 because its an integer divide, it truncates the fraction
            System.out.println(x / (double) y);// now the answer is correct because we cast (convert) the intger y into a double.
            // since an int/double returns a doube we now get the correct result.
            System.out.println(x % y);// 5, return the modulus (the remainder)

            x += 10;// shorthand for x=x+10.  x=15
            System.out.println(x);// 15
            x -= 10;// shorthand for x=x-10.  x=5
            System.out.println(x);// 5
            x *= 10;// shorthand for x=x*10.  x=50
            System.out.println(x);// 50
            x /= 10;// shorthand for x=x/10.  x=5
            System.out.println(x);// 5

            // simple menu
            System.out.println("1. Add 2 numbers");
            System.out.println("2. Subtract 2 numbers");
            System.out.println("3. Multiply 2 numbers");
            System.out.println("4. Divide 2 numbers");
            System.out.println("5. Quit");
            System.out.println("Please enter an operation to perform (1, 2, 3 or 4)");

            Scanner in = new Scanner(System.in);
            int choice = in.nextInt();
            if (choice == 1) {
                // do add
            } else if (choice == 2) {
                // do subtract
            } else if (choice == 3) {
                // do multiply
            } else if (choice == 4) {
                // do divide
            } else if (choice == 5) {
                // do quit
            }
            // >, >=, <, <=

            // instanceof operator
            String s = "";
            if (s instanceof String) {
                System.out.println("s is a String");
            }
            // bitwise operations
            char bits = 0x0f;// bits=00001111
            char mask = 0x03;// mask=00000011
            System.out.println(bits & mask);// AND : print 3
            System.out.println(bits | mask);// OR : print 15
            System.out.println(bits ^ mask);// EXCLUSIVE OR : print 12
            System.out.println(bits >> 1);// SHIFT RIGHT ONCE : print 00000111 = 7
            System.out.println(bits << 1);// SHIFT RIGHT ONCE : print 00011110 = 30
            System.out.println(~bits);// COMPLEMENT OR INVERSE :
            // print 11110000 = -16, note all numbers are signed in java
            // shift
            char num = 0x43;// 0x43=0100011
            int shiftLeft = num << 1;
            System.out.println("0x43 shifted to the left once = " + Integer.toBinaryString(new Integer(shiftLeft)));
            int shiftRight = shiftLeft >> 2;
            System.out.println("0x43 shifted to the left once then twice to the right is = " + Integer.toBinaryString(new Integer(shiftRight)));
        }

        // control flow
        // if/while, do while, switch, for, break  statements
        {// note use of blocks.  lets me reuse variable names without errors
            int x = 0;
            int y = 10;

            if (x == y) {
                System.out.println("x==y");
            }

            // if else
            if (x == y) {
                System.out.println("x==y");
            } else {
                System.out.println("x!=y");
            }

            // loops
            // loop 1
            x = 10;
            System.out.println("loop 1: while");
            while (x != 0) { // prints 10, 9, 8 ...1
                System.out.println("x=" + x);
                x--;
            }
            // loop 2 does same thing as loop 1 note x-- in expression for concise coding/readability
            x = 10;
            System.out.println("loop 2: while");
            while (x-- != 0) {// prints 9, 8, ... 0
                System.out.println("x=" + x);
            }
            // loop 3.  do - while, always executes block at LEAST ONCE!
            x = 10;
            System.out.println("loop 3 : do while");
            do {// prints 10, 9, 8 ... 0
                System.out.println("x=" + x);
            } while (x-- > 0);

            // loop 4
            System.out.println("loop 4: for");
            for (x = 0; x < 10; x++) {// prints 0 ... 9
                System.out.println("x=" + x);
            }

            // loop 5
            System.out.println("loop 5: for with break");
            for (x = 0; x < 10; x++) {// prints 0 only, break, breaks out of the loop
                System.out.println("x=" + x);
                break;
            }
            // loop 6
            System.out.println("loop 6: for with continue");
            for (x = 0; x < 10; x++) {// prints 0, 2, 4, 6, 8, skips all odd numbers becuase of the continue
                if (x % 2 > 0) {
                    continue;
                }
                System.out.println("x=" + x);
            }

            // simple menu with switch
            boolean keepGoing = true;
            while (keepGoing) { // endless loop, breakout by setting keepGoing=false
                System.out.println("1. Add 2 numbers");
                System.out.println("1. Subtract 2 numbers");
                System.out.println("3. Multiply 2 numbers");
                System.out.println("4. Divide 2 numbers");
                System.out.println("5. Quit");
                System.out.println("Please enter an operation to perform (1, 2, 3 or 4)");

                Scanner in = new Scanner(System.in);
                int choice = in.nextInt();
                switch (choice) {
                    case 1:
                        System.out.println("Adding 2 numbers");
                        break;
                    case 2:
                        System.out.println("Subtracting 2 numbers");
                        break;
                    case 3:
                        System.out.println("Multiplying 2 numbers");
                        break;
                    case 4:
                        System.out.println("Dividing 2 numbers");
                        break;
                    case 5:
                        System.out.println("Quitting");
                        keepGoing = false;
                        break;
                    default:
                        System.out.println("Error, try again");
                }
                // switch
                switch (x) {
                    case 1:
                        break;
                    case 2:
                        break;
                }
            }

        }

    }
}
