/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package _fall2018_current.lecture3.application_skeleton;

import java.util.Scanner;

/**
 *
 * @author fcarella
 */
public class App {

    Scanner input = new Scanner(System.in);

    public void run() {

        boolean quit = false;
        String mainMenu = ""
                + "1. Option 1\n"
                + "2. Option 2\n"
                + "3. Option 3\n"
                + "4. Option 4\n"
                + "5. Help\n"
                + "6. Quit\n\n"
                + "";
        while (!quit) {
            System.out.println(mainMenu);
            int choice = input.nextInt();
            switch (choice) {
                case 1:
                    doOption1();
                    break;
                case 2:
                    doOption2();
                    break;
                case 3:
                    doOption3();
                    break;
                case 4:
                    doOption4();
                    break;
                case 5:
                    help();
                    break;
                case 6:
                    quit = true;
                    break;
                default:
                    System.out.println("Invalid entry try again");

            }
        }
        System.exit(0);
    }

    private void doOption1() {
        System.out.println("Doing option 1 right now");

    }

    private void doOption2() {
        System.out.println("Doing option 2 right now");
    }

    private void doOption3() {
        System.out.println("Doing option 3 right now");
    }

    private void doOption4() {
        System.out.println("Doing option 4 right now");

    }

    private void help() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
