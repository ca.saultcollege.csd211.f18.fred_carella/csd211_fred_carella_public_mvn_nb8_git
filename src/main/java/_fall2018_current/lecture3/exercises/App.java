/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package _fall2018_current.lecture3.exercises;

import java.util.Scanner;

/**
 *
 * @author fcarella
 */
public class App {
    private Scanner input=new Scanner(System.in);

    public App() {
    }
    public void run(){
        
        int x=10;
        while(x > 0){
            System.out.println("x="+x);
            x--;
        }
        x=10;
        do{
            System.out.println("x="+x);
            x--;
        }while(x!=0);
        
        
        for(int y=0;y<10;y++){
            System.out.println("y="+y);
        }
        x=10;
        while(x>0){
            x--;
            if(x%2==0)
                continue;
            System.out.println("x="+x);
        }
    }
}
