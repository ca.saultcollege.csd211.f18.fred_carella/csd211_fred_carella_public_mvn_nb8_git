/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package _fall2018_current.lecture3;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

/**
 *
 * @author fcarella
 */
public class DateExamples {

    public static void main(String[] args) {
        // time and date formats

        // first define patterns.  These patterns determine how the date will look, May-01-2015 or 05/01/15 for example
        String datePattern = "MMM-dd-yyyy";
        String timePattern = "hh:mmaa";
        String timeAndDatePattern = "MMM-dd-yyyy hh:mmaa";

        // we need formatters that we'll use to get the dates in the proper format.  
        // Formatters are based on patterns
        SimpleDateFormat dateFormat = new SimpleDateFormat(datePattern);
        SimpleDateFormat timeFormat = new SimpleDateFormat(timePattern);
        SimpleDateFormat timeAndDateFormat = new SimpleDateFormat(timeAndDatePattern);

        System.out.println("Enter date in the format MMM-dd-yyyy");
        System.out.println("For example, it is now " + dateFormat.format(new Date().getTime()));
        Date date = getDate(dateFormat);
        System.out.println("You entered the following date "+dateFormat.format(date.getTime()));
        
        System.out.println("Enter time in the format hh:mmaa");
        System.out.println("For example, it is now " + timeFormat.format(new Date().getTime()));
        Date time = getDate(timeFormat);
        System.out.println("You entered the following time "+timeFormat.format(time.getTime()));
        
        System.out.println("Enter date and time in the format MMM-dd-yyyy hh:mmaa");
        System.out.println("For example, it is now " + timeAndDateFormat.format(new Date().getTime()));
        Date timeAndDate = getDate(timeAndDateFormat);
        System.out.println("You entered the following date and time "+timeAndDateFormat.format(timeAndDate.getTime()));
        
        
        
        if(timeAndDate.getHours()<12)
            System.out.println("Good morning...");
        
        if(timeAndDate.getHours()==12)
            System.out.println("Its now noon");
        if(timeAndDate.getHours()==0)
            System.out.println("Its now midnight");
        
        System.out.println(timeAndDate.getHours());
        
    }

    private static Date getDate(SimpleDateFormat format) {
        Date date = null;
        Scanner input = new Scanner(System.in);
        while (date == null) {
            String line = input.nextLine();
            try {
                date = format.parse(line);
            } catch (ParseException e) {
                System.out.println("Sorry, that's not valid. Please try again.");
            }
        }
        return date;
    }
}
