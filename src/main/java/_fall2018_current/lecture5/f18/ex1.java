/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package _fall2018_current.lecture5.f18;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import lab4.q2.Car;

/**
 *
 * @author fcarella
 */
public class ex1 {
    public static void main(String[] args) {
        ArrayList list=new ArrayList();
        String o1="hello1";
        list.add(o1);
        list.add("hello2");
        list.add(new String("hello3"));
        list.add(new Integer(1));
        for(int i=0;i<list.size();i++){
            System.out.println("list("+i+")="+list.get(i));
        }
        
        // this list only accepts String's
        ArrayList<String> stringList=new ArrayList<>();
        String o1_1="hello1";
        stringList.add(o1_1);
        stringList.add("hello2");
        stringList.add(new String("hello3"));
        // cant do this
        //stringList.add(new Integer(1));
        for(int i=0;i<stringList.size();i++){
            System.out.println("stringList("+i+")="+stringList.get(i));
        }
        
        List<Integer> integerList=new ArrayList<>();
        Collection<Integer> integerListCollection=new ArrayList<>();
        
        integerList.add(1);
        integerList.add(2);
        integerList.add(3);
        integerList.add(new Integer(4));
        // iterate through the list using method 1
        for(int i=0;i<integerList.size();i++){
            System.out.println("integerList("+i+")="+integerList.get(i));
        }
        // iterate through the list using method 2, for each
        for(Integer my_int:integerList){
            System.out.println("integerList-> "+my_int);
        }
        //
        List<Object> anyList=new ArrayList<>();
        anyList.add("String");
        anyList.add(new Integer(10));
        anyList.add(new Double(2.2));
        for(int i=0;i<anyList.size();i++){
            System.out.println("anyList->"+anyList.get(i));
        }
        for(Object my_obj:anyList){
            if(my_obj instanceof Integer)
                continue;
            System.out.println("anyList-> "+my_obj);
        }
        // iterate through the list using method 3
        Iterator it=anyList.iterator();
        while(it.hasNext()){
            Object o=it.next();
            System.out.println("anyList-> "+o);
        }
        
        Car mustang=new Car("Ford", "Mustang");
        List<Car> cars=new ArrayList<>();
        cars.add(mustang);
    }
}
