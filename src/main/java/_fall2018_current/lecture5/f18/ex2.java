/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package _fall2018_current.lecture5.f18;

import _fall2018_current.lecture5.ArrayListObjectPersistence;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author fcarella
 */
public class ex2 {
    public static void main(String[] args) {
        Car c1=new Car("ford", "mustang");
        ArrayList<Car> cars =new ArrayList<>();
        cars.add(c1);
        
        try {
            FileOutputStream saveFile = new FileOutputStream("ArraylistObjectPersistence.sav");
            ObjectOutputStream save = new ObjectOutputStream(saveFile);
            save.writeObject(cars);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ArrayListObjectPersistence.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ArrayListObjectPersistence.class.getName()).log(Level.SEVERE, null, ex);
        }
        ArrayList<Car> carsIn = null;
        try {
            FileInputStream saveFileIn = new FileInputStream("ArraylistObjectPersistence.sav");
            ObjectInputStream restore = new ObjectInputStream(saveFileIn);
            carsIn = (ArrayList< Car>) restore.readObject();
        } catch (Exception ex) {
            Logger.getLogger(ArrayListObjectPersistence.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("-----------------------------------");
        for(Car car:carsIn){
            System.out.println(car);
        }
        System.out.println("-----------------------------------");

    }
}
