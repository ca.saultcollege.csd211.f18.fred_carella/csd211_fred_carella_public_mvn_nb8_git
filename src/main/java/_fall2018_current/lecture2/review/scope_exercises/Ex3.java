/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package _fall2018_current.lecture2.review.scope_exercises;


/**
 *
 * @author fcarella
 */
public class Ex3 {

    public static void main(String[] args) {
        Person joe=new Person();
        joe.setFirstName("Joe");
        System.out.println("count="+Person.getCount());
        
        Person tom=new Person();
        tom.setFirstName("Tom");
        System.out.println("count="+Person.getCount());
        
        System.out.println("Joe="+joe.getFirstName());
        System.out.println("Tom="+tom.getFirstName());
        
        {
            int x=10;
        }
        
        {
            int x=10;
        }
        
        
        for(int x=10;x<20;x++){
        }
        
        for(int x=10;x<20;x++){
        }
        
    }
    
}
