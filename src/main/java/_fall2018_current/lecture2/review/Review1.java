/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package _fall2018_current.lecture2.review;

/**
 *
 * @author fcarella
 */
public class Review1 {
    
    public static void main(String[] args) {
        // primitive data types
        int x; // declare a variable of type int called "x"
        x=10;// initialize variable
        
        int y=20;// declare and initialize
        float v1=1.7f;
        double v2=1.7d;
        long v3=12345678923L;
//        float v4=1.7d;
        
        
        // object types
        Integer io;// creates a reference called io
                   // points to an Integer object
                   // which is not created yet
        Integer io2=new Integer(20);// create an Integer object
                            // and a refernce called io2 that points to the object
        Float f1=new Float(1.2);
        Double d1=new Double(2.5);
        
    }
    
}
