package _fall2018_current.lecture2;

public class Variables_01 {
    private int instanceVariable;   // this is an instance variable, every instance of the class has its OWN copy.
    private static int count;       // this is a class variable, 
                                    // every instance of this class (there can be many instances at any given time) 
                                    // has access to this SINGLE variable.
    
    public static void main(String[] args) {
        int localVariable;// this variable is local the the main method, you cant access this from the "callMeMethod" method.
    }
    
    public static void callMeMethod(String parameter1){
        String s=parameter1;
        System.out.println(s);
    }
}
