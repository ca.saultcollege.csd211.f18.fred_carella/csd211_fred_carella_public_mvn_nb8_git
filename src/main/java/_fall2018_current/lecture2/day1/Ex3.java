/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package _fall2018_current.lecture2.day1;

/**
 *
 * @author fcarella
 */
public class Ex3 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        Car mustang=new Car();
        
        mustang.setYear(2010);
        mustang.setMake("Ford");
        
        Car camaro=new Car();
        camaro.setYear(2018);
        camaro.setMake("Chevy");
        
        
        System.out.println("make="+mustang.getMake() );
        System.out.println("make="+camaro.getMake() );
        System.out.println("Number of cars="+Car.count);
        
    }
    
}





