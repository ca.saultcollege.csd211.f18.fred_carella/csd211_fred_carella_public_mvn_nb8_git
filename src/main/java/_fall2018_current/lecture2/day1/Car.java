/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package _fall2018_current.lecture2.day1;

/**
 *
 * @author fcarella
 */
public class Car {
    // class variables
    private int year;
    private String make;
    public static int count;
    public int count2;
    
    public Car(){
        count++;
    }

    public int getYear(){
        return year;
    }
    
    public String getMake(){
        return make;
    }
    
    public void setYear(int year){ // year is local to the method
        this.year=year;
    }
    
    public void setMake(String make){ // make is local to the method
        this.make=make;
    }
    
    
}
