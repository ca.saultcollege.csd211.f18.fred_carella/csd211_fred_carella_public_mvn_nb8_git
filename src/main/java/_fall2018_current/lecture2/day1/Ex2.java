package _fall2018_current.lecture2.day1;

public class Ex2 {

    private int x;  // another variable called x
                    // called an instance variable or a field
    
    
    // main is a method
    public static void main(String[] args) {
        int x; // x is a variable thats local to the main method
    }
        
    
    private static void method2(){
        int x; // this is a second variable called x
                // and its local to the second method
                // they dont conflict
    }
    
}
