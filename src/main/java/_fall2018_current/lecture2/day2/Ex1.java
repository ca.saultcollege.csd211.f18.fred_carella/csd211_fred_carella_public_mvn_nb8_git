/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package _fall2018_current.lecture2.day2;

import java.util.Scanner;

/**
 *
 * @author fcarella
 */
public class Ex1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // INPUT
        double PI=3.14;
        int radius;
        double area;
        Scanner input = new Scanner(System.in);
        System.out.println("Enter the radius of the circle:");
        radius=input.nextInt();
        
        
        // PROCESS
        area=PI*radius*radius;
        
        
        // OUTPUT
        System.out.println("The area of a circle with radius "+radius+" = "+area);
        
    }
    
}
