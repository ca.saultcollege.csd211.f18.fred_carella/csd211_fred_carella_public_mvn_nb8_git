/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package _fall2018_current.lecture1.ex1;

/**
 *
 * @author fcarella
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        // "george" is a reference to a object of type Person
        Person george=new Person();
        Person thomas=new Person();
        
        george.setFirstname("George");
        george.setLastname("Washington");
        
        thomas.setFirstname("Thomas");
        thomas.setLastname("Jefferson");
        
        george.speak();
        thomas.speak();
        
        // me is a reference to a person but it doesnt 
        // point to an object yet.
        Person me;
        
        me=new Person();
        me.setFirstname("Fred");
        me.setLastname("Carella");
        
        me.speak();
        
        me=null;
        
        me.speak();
        
        
        
    }
    
}
