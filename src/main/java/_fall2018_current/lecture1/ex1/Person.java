/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package _fall2018_current.lecture1.ex1;

/**
 *
 * @author fcarella
 */
public class Person {
    /*  - a class is a template for an object
        - an object is made up of attributes (properties) and behaviours (methods)
        
    */
    
    // attributes
    private int age;
    private String firstname;
    private String lastname;
    
    // constructor
    public Person(){
        
    }
    
    public void speak(){
        System.out.println("my name is: "+ firstname + " " + lastname);
    }
    
    public void setFirstname(String firstname){
        this.firstname=firstname;
    }
    
    public void setLastname(String lastname){
        this.lastname=lastname;
    }
    
    public void setAge(int age){
        this.age=age;
    }
    
    public String getFirstname(){
        return firstname;
    }
    
    public String getLastname(){
        return lastname;
    }
    
    public int getAge(){
        return age;
    }
    
    
    
}
