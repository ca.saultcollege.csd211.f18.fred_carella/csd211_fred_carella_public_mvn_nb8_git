/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package unit_test.object_equality;

/**
 *
 * @author fcarella
 */
public class Person {
    private String firstname;
    private String lastname;
    private SocialInsuranceNumber sin;

    public Person() {
    }

    public Person(String firstname, String lastname, SocialInsuranceNumber sin) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.sin = sin;
    }

    @Override
    public boolean equals(Object obj) {
        if( !(obj instanceof Person)){
            return false;
        }
        Person p;
        p=(Person)obj;// cast obj to person
        if(p.getSin().equals(this.getSin()))
            return true;
        else
            return false;
    }

    
    
    /**
     * @return the firstname
     */
    public String getFirstname() {
        return firstname;
    }

    /**
     * @param firstname the firstname to set
     */
    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    /**
     * @return the lastname
     */
    public String getLastname() {
        return lastname;
    }

    /**
     * @param lastname the lastname to set
     */
    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    /**
     * @return the sin
     */
    public SocialInsuranceNumber getSin() {
        return sin;
    }

    /**
     * @param sin the sin to set
     */
    public void setSin(SocialInsuranceNumber sin) {
        this.sin = sin;
    }
    
    
    
}
