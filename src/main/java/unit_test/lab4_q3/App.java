/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package unit_test.lab4_q3;

import java.util.Scanner;

/**
 *
 * @author fcarella
 */
public class App {
    Scanner input=new Scanner(System.in);
    private Car[] cars=new Car[100];// this does NOT create 100 cars
    private int currentCar=0;
    
    public void run(){
        boolean done=false;
        
        while (!done){
            printMenu();
            int choice=input.nextInt();
            switch(choice){
                case 1:
                    doAdd();
                    break;
                case 2:
                    doDelete();
                    break;
                case 99:
                    done=true;
                    break;
                default:
                    System.out.println("Invalid choice try again");
            }
        }
        
        
        printCars();
        
        
    }
    private void printMenu(){
        System.out.println("1. Add");
        System.out.println("2. Delete");
        System.out.println("99. Exit");
        System.out.println("Please enter a choice:");
    }
    private void printCars(){
        for(int i=0; i<=cars.length-1;i++){
            if(cars[i]!=null)
                System.out.println("Cars["+i+"]=="+cars[i]);
        }
    }

    private void doAdd() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void doDelete() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
