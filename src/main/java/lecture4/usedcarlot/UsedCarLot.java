/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package lecture4.usedcarlot;

import java.util.Scanner;
import lecture4.car.Car;
import lecture4.l1q1.Person;

/**
 *
 * @author fcarella
 */
public class UsedCarLot {
    private static Car[] cars=new Car[100];
    private static int currentCar=0;
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String menu="" +
                "\n\n1. Add a car\n" +
                "2. Sell a car\n" +
                "3. List all cars\n" +
                "4. Delete a car\n" +
                "5. Edit a car\n"+
                "6. Quit";

        boolean quit=false;
        Scanner input=new Scanner(System.in);

        while(!quit){
            System.out.println(menu);
            int option=input.nextInt();

            switch(option){
                case 1:
                    try {
                        addACar();
                    } catch (Exception ex) {
                        System.out.println(ex.getMessage());
                    }
                    break;
                case 2:
                    sellACar();
                    break;
                case 3:
                    listAllCars();
                    break;
                case 4:
                    deleteACar();
                    break;
                case 5:
                    editCar();
                    break;
                case 6:
                    quit=true;
                    break;
                default:
                    System.out.println("Invalid entry, try again...");
            }

        }

    }

    @Override
    public String toString() {

        String s="Used Car Lot\n";

        for(Car car:cars){
            s+=car.toString();
            s+="\n";
        }
        return s;

    }

    private static void addACar() throws Exception {
        Car car=new Car();
        if(currentCar==100)
            throw new Exception("Car lot is full");

        boolean quit=false;
        while(!quit){
            Scanner input=new Scanner(System.in);
            System.out.println("Enter make (-1 to quit): ");
            String make = input.next();
            if(make.equals("-1")){
                quit=true;
                break;
            }
            System.out.println("Enter model: ");
            String model = input.next();
            System.out.println("Enter year: ");
            int year = input.nextInt();

            Car c=new Car(make, model, year);

            cars[currentCar++]=c;
        }
    }

    private static void editCar() {

    }

    private static void deleteACar() {

    }

    private static void listAllCars() {
        for(Car car : cars){
            if( car == null )
                break;
            System.out.println(car);
        }
    }

    private static void sellACar() {
        Person owner=getOwner();

    }

    private static Person getOwner() {
        Scanner input=new Scanner(System.in);
        System.out.println("Enter First Name: ");
        String fname = input.next();
        System.out.println("Enter Last Name: ");
        String lname = input.next();
        Person p=new Person(fname, lname);
        return p;
    }

}
