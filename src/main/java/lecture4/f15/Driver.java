/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lecture4.f15;

/**
 *
 * @author fcarella
 */
public class Driver {
    public static void main(String[] args) {
        String name=new String("Fred");
        String lastName=new String();
        
        Integer int1=new Integer(1);
        
        Person joe=new Person();
        joe.setFirstname("Joe");
        joe.setLastname("Eder");
        joe.setAge(21);
        
        Person peter=new Person("Joe", "Eder", 21);
        
        System.out.println(peter.getFirstname()+" "+peter.getLastname()+" "+peter.getAge());
        System.out.println(peter);
        
//        peter=joe;
        if(peter.equals(joe)){
            System.out.println("Peter == Joe");
        }else
            System.out.println("Peter != Joe");
    }
}
