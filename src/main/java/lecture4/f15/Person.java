/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lecture4.f15;

/**
 *
 * @author fcarella
 */
public class Person {

    // instance variables

    private String firstname;
    private String lastname;
    private int age;

    // constructors
    public Person() {
        this.firstname = "Not initialized yet";
        this.lastname = "Not initialized yet";
        this.age = 0;
    }

    public Person(String firstname, String lastname, int age) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.age = age;
    }

    public String toString() {
        return getFirstname() + " " + getLastname() + " " + getAge();
    }

    public boolean equals(Object o) {
        if (!(o instanceof Person)) {
            return false;
        }
        Person p = (Person) o;
        if (p.getFirstname() == firstname
                && p.getLastname() == lastname
                && p.getAge() == age) {
            return true;
        }
        return false;

    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        if (age <= 0) {
            age = 1;
        }
        this.age = age;
    }

}
