/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lecture4.f15;

import java.util.Scanner;

/**
 *
 * @author fcarella
 */
public class App {

    private Scanner input = new Scanner(System.in);

    public void run() {
        System.out.println("Menu Loop Demo");
        System.out.println("--------------");
        boolean quit = false;
        int x = 0;
        while (!quit) {
            System.out.println("1. Add Person");
            System.out.println("2. Do option 2");
            System.out.println("99. Quit");
            System.out.println("Enter a menu option please");
            int choice = input.nextInt();
            /*
             if(choice==1){
             // do option 1
             }else
             if(choice==2){
             // do option 2
             }else
             if(choice==99){
             // do option quit
             }
             */
            switch (choice) {
                case 1: // do option 1
                    addPerson();
                    break;
                case 2: // do option 2
                    doOption2();
                    break;
                case 99: // do quit
                    System.out.println("Quitting");
                    quit = true;
                    break;
                default:
                    System.out.println("You entered a wrong option, please try again");
            }

        }
    }

    private void addPerson() {
        System.out.println("Add a Person");
        boolean quit = false;
        while (!quit) {
            System.out.println("Enter first name");
            String firstname = input.next();
            System.out.println("Enter last name");
            String lastname = input.next();
            int age=0;
            try {
                System.out.println("Enter age");
                age = input.nextInt();
            } catch (Exception e) {
                System.out.println("Incorrect Entry, try again");
            }
            Person p = new Person(firstname, lastname, age);
            System.out.println("You entered Person " + p);
            System.out.println("Press 'q' to quit");
            String quitS = input.next();
            if (quitS.equals("q") || quitS.equals("Q")) {
                quit = true;
            }
        }
    }

    private void doOption2() {
        System.out.println("Executing option 2");
    }
}
