/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package lecture4.l1q1;

/**
 *
 * @author fcarella
 */
public class Person {
    // Attributes of a person
    // defined as instance variables
    private int age; //
    private double heightCm; // in cm
    private double weightKg ;//  in kg
    private String firstname;
    private String lastname;
    private char gender;

    Person(String string, String string0, int i, char c) {
        firstname=string;
        lastname=string0;
        age=i;
        gender=c;
    }
    Person(String string, String string0, int i, double d, char c) {
        this(string, string0, i, c);
        heightCm=d;
    }

    Person(String string, String string0, int i, double d, double d0, char c) {
        this(string, string0, i, c);
        heightCm=d;
        weightKg=d0;
    }


    /**
     * @return the age
     */
    public int getAge() {
        return age;
    }

    /**
     * @param age the age to set
     */
    public void setAge(int age) {
        this.age = age;
    }


    /**
     * @return the firstname
     */
    public String getFirstname() {
        return firstname;
    }

    /**
     * @param firstname the firstname to set
     */
    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    /**
     * @return the lastname
     */
    public String getLastname() {
        return lastname;
    }

    /**
     * @param lastname the lastname to set
     */
    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    /**
     * @return the gender
     */
    public char getGender() {
        return gender;
    }

    /**
     * @param gender the gender to set
     */
    public void setGender(char gender) {
        this.gender = gender;
    }

    public Person() {
    }
    public Person(String fname, String lname) {
        firstname=fname;
        lastname=lname;
    }


    @Override
    public String toString() {
        String s="Name : ";
        s=s+firstname+" "+lastname+"\n";
        s+="Age : ";
        s+=age+"\n";
        s+="Height : ";
        s+=heightCm+"cm \n";
        s+="Weight : ";
        s+=weightKg+"kg \n";
        s+="Gender : ";
        s+=gender+"\n";

        return s;
    }

    /**
     * @return the heightCm
     */
    public double getHeightCm() {
        return heightCm;
    }

    /**
     * @param heightCm the heightCm to set
     */
    public void setHeightCm(double heightCm) {
        this.heightCm = heightCm;
    }

    /**
     * @return the weightKg
     */
    public double getWeightKg() {
        return weightKg;
    }

    /**
     * @param weightKg the weightKg to set
     */
    public void setWeightKg(double weightKg) {
        this.weightKg = weightKg;
    }


}
