/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lecture4.l1q1;

/**
 *
 * @author fcarella
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Person p1 = new Person("Joe", "Student", 23, 'M');// Joe Student is 23 years old and male.
        Person p2 = new Person("George", "Student", 24, 188.2, 'M');// George Student is 24 years old, male,  is 182.88 cm tall and weighs 60 kg.
        Person p3 = new Person("Mary", "Teacher", 43, 170, 48.2, 'F');// Mary Teacher is female, 43 years old, is 170.00 cm tall and weighs 48.2 kg.    }

        System.out.println(p1);
        System.out.println(p2);
        System.out.println(p3);
    }
}
