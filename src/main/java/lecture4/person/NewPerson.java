/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package lecture4.person;

/**
 *
 * @author fcarella
 */
public class NewPerson {
    private int age;
    private String firstname;
    private String lastname;
    private char gender;

    public NewPerson() {
        firstname="no first name entered";
        lastname="no last name entered";
        age=15;
    }

    public NewPerson(String firstname, String lastname) {
        this.firstname=firstname;
        this.lastname=lastname;
    }

    @Override
    public boolean equals(Object obj) {
        return false;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        return hash;
    }

    @Override
    public String toString() {
        String s="";
        s+="Firstname : ";
        s+=firstname;
        s+="\n";
        s+="Lastname : ";
        s+=lastname;
        s+="\n";
        s+=age;
        s+=gender;


        return s;
    }



    /**
     * @return the firstname
     */
    public String getFirstname() {
        return firstname;
    }

    /**
     * @param firstname the firstname to set
     */
    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    /**
     * @return the lastname
     */
    public String getLastname() {
        return lastname;
    }

    /**
     * @param lastname the lastname to set
     */
    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    /**
     * @return the gender
     */
    public char getGender() {
        return gender;
    }

    /**
     * @param gender the gender to set
     */
    public void setGender(char gender) {
        this.gender = gender;
    }

    /**
     * @return the age
     */
    public int getAge() {
        return age;
    }

    /**
     * @param age the age to set
     */
    public void setAge(int age) {
        this.age = age;
    }

}
