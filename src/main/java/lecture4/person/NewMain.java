/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package lecture4.person;


/**
 *
 * @author fcarella
 */
public class NewMain {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        NewPerson noel=new NewPerson("Noel", "Hall");
        NewPerson gille=new NewPerson("Noel", "Hall");
        System.out.println("Noel is "+noel.getAge() +" years old");

        System.out.println("All about Noel: "+noel);

        if((noel.equals(gille)))
            System.out.println("Noel and Gilles are the same person!");
        else
            System.out.println("Noel and Gilles are NOT the same person!");
    }

}
