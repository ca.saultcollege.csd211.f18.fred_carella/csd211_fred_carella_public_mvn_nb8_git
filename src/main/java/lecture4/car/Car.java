/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package lecture4.car;

import lecture4.l1q1.Person;

/**
 *
 * @author fcarella
 */
public class Car {
    private String make;
    private String model;
    private int year;
    private Person owner;

    public Car() {
    }

    public Car(String make, String model, int year) {
        this.make = make;
        this.model = model;
        this.year = year;
    }

    public Car(String make, String model, int year, Person owner) {
        this.make = make;
        this.model = model;
        this.year = year;
        this.owner = owner;
    }

    @Override
    public String toString() {
        String s="";
        s+= "\nMake :" + make+
            "\nModel : " + model +
            "\nYear: " + year;
        Person owner=getOwner();
        if(owner!=null)
            s+=owner.toString();
        return s;
    }

    /**
     * @return the make
     */
    public String getMake() {
        return make;
    }

    /**
     * @param make the make to set
     */
    public void setMake(String make) {
        this.make = make;
    }

    /**
     * @return the model
     */
    public String getModel() {
        return model;
    }

    /**
     * @param model the model to set
     */
    public void setModel(String model) {
        this.model = model;
    }

    /**
     * @return the year
     */
    public int getYear() {
        return year;
    }

    /**
     * @param year the year to set
     */
    public void setYear(int year) {
        this.year = year;
    }

    /**
     * @return the owner
     */
    public Person getOwner() {
        return owner;
    }

    /**
     * @param owner the owner to set
     */
    public void setOwner(Person owner) {
        this.owner = owner;
    }
}
