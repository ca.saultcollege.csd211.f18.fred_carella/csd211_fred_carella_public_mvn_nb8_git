/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package lecture4.exercises;

/**
 *
 * @author fcarella
 */
public class Person extends Object{
    private String firstName;
    private String lastName;

    public Person() {
        firstName="Not initialized yet";
        lastName="Not initialized yet";
    }

    public Person(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public Person(String firstName) {
        this();
        this.firstName=firstName;
    }

    @Override
    public String toString() {
        return firstName+" "+lastName;
    }


    public String getFirstName(){
        return firstName;
    }
    public void setFirstName(String firstName){
        this.firstName=firstName;
    }

    /**
     * @return the lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * @param lastName the lastName to set
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

}
