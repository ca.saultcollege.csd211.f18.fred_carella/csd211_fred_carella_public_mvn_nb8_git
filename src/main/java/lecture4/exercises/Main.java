/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package lecture4.exercises;

/**
 *
 * @author fcarella
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Person noel=new Person();
        noel.setFirstName("Noel");
        noel.setLastName("Hall");
        System.out.println(noel);

        Person gilles=new Person("Gilles");
        System.out.println(gilles);

        Person laRock=new Person("Gilles","LaRock");
        System.out.println(laRock);
    }

}
