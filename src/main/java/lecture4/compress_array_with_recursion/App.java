/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lecture4.compress_array_with_recursion;

/**
 *
 * @author fcarella
 */
public class App {

    private String[] strings = new String[10];
    private int j=0;
    public void run() {
        populateArray();
        strings[1] = null;
        strings[3] = null;
        strings[5] = null;
        printArray();
        j=0;
        compress(0);
        printArray();
        
        populateArray();
        strings[7] = null;
        strings[8] = null;
        strings[9] = null;
        printArray();
        j=0;
        compress(0);
        printArray();
        
        populateArray();
        strings[0] = null;
        strings[1] = null;
        strings[9] = null;
        printArray();
        j=0;
        compress(0);
        printArray();
    }

    private void printArray() {
        System.out.println("-------------");
        for (int i = 0; i < strings.length; i++) {
            String s = (strings[i] == null) ? "null" : strings[i];
            System.out.println("strings[i]=" + s);
        }
    }

    private void populateArray() {
        for (int i = 0; i < strings.length; i++) {
            strings[i] = new String("String " + i);
        }
    }

    private void compress(int i) {
        if(j>=strings.length-1) return;
            while (i != strings.length - 1 && j<strings.length - 1 ) {
                if (strings[i] == null && strings[i + 1] != null) {
                    strings[i] = strings[i + 1];
                    strings[i + 1] = null;
                }
                compress(i + 1);
            }
            j++;
            compress(0);
        }

}
