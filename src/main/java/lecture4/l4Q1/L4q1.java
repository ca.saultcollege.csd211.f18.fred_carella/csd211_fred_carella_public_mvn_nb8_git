/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package lecture4.l4Q1;

/**
 *
 * @author 11033716
 */
public class L4q1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here

        Person p1=new Person("Joe", "Student", 23, 'M');// Joe Student is 23 years old and male.
        Person p2 = new Person("George", "Student", 24, 188.2, 60.0, 'M');// George Student is 24 years old, male,  is 182.88 cm tall and weighs 60 kg.
        Person p3 = new Person("Mary", "Teacher", 43, 170, 48.2, 'F');// Mary Teacher is female, 43 years old, is 170.00 cm tall and weighs 48.2 kg.
        System.out.println(p1);
        System.out.println(p2);
        System.out.println(p3);

        if(p1.equals(p2) || p1.equals(p3)){
            System.out.println("the values are the same");
        }
        else if(p2.equals(p1) || p2.equals(p3)){
            System.out.println("the values are the same");
        }
        else if(p3.equals(p1) || p3.equals(p2)){
            System.out.println("the values are the same");
        }
        else
            System.out.println("the values are not the same");
    }

}
