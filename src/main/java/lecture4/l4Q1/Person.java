/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package lecture4.l4Q1;

/**
 *
 * @author 11033716
 */
public class Person {

    private String firstname;
    private String occupation;
    private int age;
    private char gender;
    private double height;
    private double weight;

    public Person(String firstname, String occupation, int age, char gender) {
      this.firstname = firstname;  
      this.occupation = occupation;
      this.age = age;
      this.gender = gender;
    }
    
    public Person(String firstname, String occupation, int age, double height, double weight, char gender){

      this.firstname = firstname;
      this.occupation = occupation;
      this.age = age;
      this.height = height;
      this.weight = weight;
      this.gender = gender;
    }
    
    @Override
    public String toString(){
        return "Name: " + firstname + " " + occupation
                +"\nAge: " + age
                +"\nheight: " + height
                +"\nweight: " + weight
                +"\ngender: " + gender;
    }
    
    @Override
    public boolean equals(Object obj){
     
        Person obj2 = (Person) obj;
        
        if (this.firstname.equals(obj2.getFirstname())){
            if(this.occupation.equals(obj2.getOccupation())){
                if(this.age == obj2.getAge()){
                    if(this.height == obj2.getHeight()){
                        if(this.weight == obj2.getWeight()){
                            if(this.gender == obj2.getGender()){
                                return true;

                            }
                       }
                    }
                }
            }
        }
        
        return false;
    }

    /**
     * @return the firstname
     */
    public String getFirstname() {
        return firstname;
    }
    /**
     * @param firstname the firstname to set
     */
    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    /**
     * @return the occupation
     */
    public String getOccupation() {
        return occupation;
    }

    /**
     * @param occupation the occupation to set
     */
    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    /**
     * @return the age
     */
    public int getAge() {
        return age;
    }

    /**
     * @param age the age to set
     */
    public void setAge(int age) {
        this.age = age;
    }

    /**
     * @return the gender
     */
    public char getGender() {
        return gender;
    }

    /**
     * @param gender the gender to set
     */
    public void setGender(char gender) {
        this.gender = gender;
    }

    /**
     * @return the height
     */
    public double getHeight() {
        return height;
    }

    /**
     * @param height the height to set
     */
    public void setHeight(double height) {
        this.height = height;
    }

    /**
     * @return the weight
     */
    public double getWeight() {
        return weight;
    }

    /**
     * @param weight the weight to set
     */
    public void setWeight(double weight) {
        this.weight = weight;
    }


}
