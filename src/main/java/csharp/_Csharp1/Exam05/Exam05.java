/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package _Csharp1.Exam05;

import java.util.Scanner;


/**
 *
 * @author fred
 */
public class Exam05 {
    static Scanner input=new Scanner(System.in);
    public static void main(String[] args) {
            System.out.print("Enter radius: ");
            double r = input.nextDouble();
            double p = r * 2 * Math.PI;
            double a = r * r * Math.PI;
            System.out.printf("Perimeter and area of a circle with radius %2.4f: %2.4f, %2.4f", r, p, a);
        }

}
