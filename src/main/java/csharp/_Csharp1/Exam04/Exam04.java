/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package _Csharp1.Exam04;


/**
 *
 * @author fred
 */
public class Exam04 {

    public static void main(String[] args)         {
            int n = 7;
            System.out.println("n = " + n);
            System.out.println("n++ = " + n++);
            System.out.println("++n = " + ++n);
            System.out.println("n-- = " + n--);
            System.out.println("--n = " + --n);
        }

}
