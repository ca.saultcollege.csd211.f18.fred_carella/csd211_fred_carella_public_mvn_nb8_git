/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package _Csharp1.Exam02;

/**
 *
 * @author fred
 */
public class Exam02 {

    public static void main(String[] args) {
        Title();
        More();
    }

    private static void More() {
        System.out.println("2. Edition");
        System.out.println("Published 2003");
        System.out.println("179 Pages");
    }

    private static void Title() {
        System.out.println("Vine fra Alsace");
        System.out.println("Soren Frank");
        System.out.println("ISBN: 87-7901-152-7");
        System.out.println("Montergarden");
    }

}
