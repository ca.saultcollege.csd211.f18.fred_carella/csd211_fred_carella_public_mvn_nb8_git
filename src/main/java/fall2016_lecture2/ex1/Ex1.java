/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fall2016_lecture2.ex1;

/**
 *
 * @author fcarella
 */
public class Ex1 {

    private final static double PI=3.14; // constant
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // Declare two variables
        int x; // x is a variable of type "int" (whole number, no fractions)
        double y; // y is a variable of type "double" (real number, can have a fractional part)
        float z; // float is also a real number data type (smaller range than a double)
        
        double pisquared=Math.PI*Math.PI;
        
//        x=1.2; // you cant do this.  The type on the left must equal the type on the right

        x=10;
        y=1.2;
//        z=1.2;  // error because 1.2 is a double and z is a float
        z=1.2f;
        z=(float)1.2; // legal because we "cast" the double into a float
        
        y=1.2f;
        
        byte b=127;// declare and initialize
        short c=32767;
        long d;
        
        Integer yy=new Integer(10);
        
        
    }
    
}
