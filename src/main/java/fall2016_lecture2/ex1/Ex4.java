/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fall2016_lecture2.ex1;

import java.util.Scanner;

/**
 *
 * @author fcarella
 */
public class Ex4 {
    public static void main(String[] args) {
        Scanner input=new Scanner(System.in);
        
        // INPUT
        String firstname="Joe"; // hard coded
        
        // PROCESS
        Person joe=new Person();
        joe.setFirstName(firstname);
        
        System.out.println("Please enter a name:");
        firstname=input.next();
        Person anon=new Person();
        anon.setFirstName(firstname);
        
        // OUTPUT
        System.out.println(joe.getFirstName());
        System.out.println(anon.getFirstName());
        System.out.println("There are "+Person.getCount()+" people in this system");
    }
    
}
