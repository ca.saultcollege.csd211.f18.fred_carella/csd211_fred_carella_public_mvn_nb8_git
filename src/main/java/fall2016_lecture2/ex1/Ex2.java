/*
 */
package fall2016_lecture2.ex1;

public class Ex2 {
    private static int x=10; // instance variable
    
    public static void main(String[] args) {
        int x=12;
        System.out.println(x);
        printX();
        printX(x);
    }
    
    public static void printX(){
        System.out.println(x);
    }
    
    public static void printX(int x){
        System.out.println(x);
    }
}
