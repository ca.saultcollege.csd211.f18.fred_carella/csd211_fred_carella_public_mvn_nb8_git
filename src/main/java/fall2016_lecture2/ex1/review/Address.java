/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fall2016_lecture2.ex1.review;

/**
 *
 * @author fcarella
 */
public class Address {

    // instance variables
    // every object has a copy
    private String street;
    private String city;
    private String postalCode;
    private int x=2;

    public String getStreet() {
        setX(12);
        
        return street;
    }

    public void setStreet(String s) {
        street = s;
    }

    public int getX() {
        return x;
    }

    /**
     * @param x the x to set
     */
    public void setX(int x) {
        this.x = x;
    }

}
