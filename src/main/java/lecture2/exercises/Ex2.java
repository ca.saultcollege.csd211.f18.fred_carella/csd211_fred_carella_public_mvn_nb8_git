/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lecture2.exercises;

/**
 *
 * @author fcarella
 */
public class Ex2 {
    private static int result;
    private static int i1;
    
    public static void main(String[] args) {
        int i1;
        int i2;
        
        // input
        i1=10;
        i2=13;
        // process
        result=i1+i2;
        
        result=add(i1,i2);
        // output
        System.out.println("Add example: "+i1+" + "+i2+" = "+result);

    }

    public static int add(int x, int y) {
        int sum = x + y;
        return sum;
    }
    
    public static int mul(int x, int y){
        int result=0;
        result=x*y;
        return result;
    }
    
    public static int div(int x, int y){
        int result=0;
        result=x/y;
        return result;
    }
    
    public static int sub(int x, int y){
        int result=0;
        result=x-y;
        return result;
    }
}
