/*
 * Exercise 1
 * 
 * 
 * Author :         Fred Carella
 * Date:            Sep 24, 2015
 * Description :    an exercise in input/process/output
 * 
*/
package lecture2.exercises;

/**
 *
 * @author fcarella
 */
public class Ex1 {
    public static void main(String[] args) {
        // input
        int radius=10;
        double PI=3.14;
        PI=Math.PI;
        double area;
        // process
        area=PI*radius*radius;
        // output
        System.out.println("The area of the circle with radius "+radius+" is "+area);
        
    }
    
}
