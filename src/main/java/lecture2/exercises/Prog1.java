/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lecture2.exercises;

/**
 *
 * @author fcarella
 */
public class Prog1 {
    public static void main(String[] args){
        System.out.println("Hello World!");
        
        // here we declare a variable called x and initialize it
        // to 10.  x is of type "int".  "int" is called a primitive data type
        int x=10;
        
        int y;  // declare a variable called y. We dont initialize it
                // but the system initializes it to a default of 0.
        y=12;   // set y to a value of 12
        // NOTE** integers can contain whole numbers only.  NO FRACTIONAL PARTS!
        // y=12.2;  // SEE Error!
        
        double realNum; // declaring a variable called realNum, its of type 
                        // "double".  "double" is a primitive data type,
                        // and numbers of type double may contain a fractional 
                        // part
        realNum=10.2;   // set realNum to 10.2
        
        float realNum2=19.0f;
        
        
        // NOTE** in general, the "left side should equal the right side"
        // We should put integers into variables declared as integers and floats 
        // into variables declared as floats, etc...
        
        realNum=19; // no error because we do not lose information
                    // when we put an integer into a real type
        // x=19.2; // error because we would lose information when putting a
                // real number into an integer
        
        x=(int)19.2; // here we cast a real number into an integer.  
                     // This is allowed.
        
        double dd=9.2d;
        float ff=9.2f;
        short sh=-32768;
    }
}
