package lecture2.exercise2;

public class AbcDriver {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Abc abc1=new Abc();
		abc1.setName("abc1");
		
		Abc abc2=new Abc("abc2");
		
		System.out.println("abc1="+abc1.getName());
		System.out.println("abc2="+abc2.getName());
		System.out.println("I have "+Abc.getCount()+" abc's created");

	}

}
