package lecture2.exercise2;

public class Abc {
	private String name;// instance variable.  each object gets its own copy
	private static int count;// static. one copy for all objects in the class
	
	public Abc(){
		count++;
	}
	
	public Abc(String name){
		this.name=name;
		count++;
	}

	public String getName(){
		return name;
	}
	
	public void setName(String name){
		this.name=name;
	}

	public static int getCount() {
		return count;
	}

	public static void setCount(int count) {
		Abc.count = count;
	}
}
