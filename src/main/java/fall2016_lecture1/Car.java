package fall2016_lecture1;

public class Car{
    private String make;
    private String model;
    public Car() {
    }
    
    public void setMake(String make){
        this.make=make;
    }
    public void setModel(String model){
        this.model=model;
    }

    /**
     * @return the make
     */
    public String getMake() {
        return make;
    }

    /**
     * @return the model
     */
    public String getModel() {
        return model;
    }
}
