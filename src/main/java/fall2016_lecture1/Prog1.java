package fall2016_lecture1;

public class Prog1{
    public static void main(String[] args){
        System.out.println("Hello World!!");
        Car mustang=new Car();
        mustang.setMake("Ford");
        mustang.setModel("Mustang");
        
        System.out.println(mustang.getMake());
        System.out.println(mustang.getModel());
        
        Car volt=new Car();
        System.out.println(volt);
        System.out.println(volt.getMake());
    }
}