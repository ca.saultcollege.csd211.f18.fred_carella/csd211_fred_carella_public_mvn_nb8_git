package ch01.ex23;


public class Driver {
    
    public static void main(String[] args) {
        Student gilles=new Student("Gilles Laroque","0");
        Student noel=new Student("Noel Hall","1");
        Student ken=new Student("Ken Whitfield","2");
        Student anthony=new Student("Anthony Bernardo","3");
        Student shane=new Student("Shane Mollari","4");
        Student extra=new Student("Extra Student","5");
        
        /* My students added */
        Student s1 = new Student("First Student", "A01010");
        Student s2 = new Student("Second Student", "A2020");
        Student s3 = new Student("Third Student", "A3030");

        LabClass labClass = new LabClass(9);
        labClass.setInstructor("Fred Carella");
        labClass.setRoom("B1055");
        labClass.setTime("Mondays @ 2:00-4:00");
        labClass.enrollStudent(gilles);
        labClass.enrollStudent(noel);
        labClass.enrollStudent(ken);
        labClass.enrollStudent(anthony);
        labClass.enrollStudent(shane);
      
        /* The added students enrolled */
        labClass.enrollStudent(s1);
        labClass.enrollStudent(s2);
        labClass.enrollStudent(s3);
        
        labClass.numberOfStudents();
        labClass.printList();
        labClass.enrollStudent(extra);
        labClass.numberOfStudents();
        labClass.printList();
    }

}
