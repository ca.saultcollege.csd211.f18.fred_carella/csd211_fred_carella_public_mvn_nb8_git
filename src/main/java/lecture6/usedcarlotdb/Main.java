package lecture6.usedcarlotdb;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author fcarella
 */
public class Main {

    private static String USERNAME = "root";
    private static String IP = "localhost";
    private static String PASSWORD = "itstudies12345";
    private static String dbNameArg = "usedcarlot";
    private static Connection con = null;
    private static String TABLE_NAME = "car";

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        System.out.println("----------------------------------");
        System.out.println("Assumes database 'usedcarlot' with empty tables exists");
        System.out.println("see usedvarlot6.mwb to create it");
        System.out.println("----------------------------------");
        try {
            // get connection.  This connection may or may not be connected to a database
            // Depends on if one exists or not
            // If it doesn't exist we get a connection with which we can create a database.
            try {
                con = getConnection();
            } catch (SQLException ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                System.exit(0);
            }


            populateTable();

            viewTable();

        } catch (SQLException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public static void viewTable() throws SQLException {
        Statement stmt = null;
        String query
                = "SELECT * from car,person where car.person_idperson=person.idperson";
        try {
            stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {
                int id = rs.getInt("idcar");
                String make = rs.getString("make");
                String firstname = rs.getString("firstname");
                System.out.println(id + "Car: " + make + " Owner: " + firstname);
            }
        } finally {
            stmt.close();
        }
    }

    public static void populateTable() throws SQLException {
        Statement stmt = null;
        try {
            stmt = con.createStatement();

            stmt.executeUpdate("INSERT INTO person (`idperson`,`firstname`,`lastname`) VALUES (1,'Fred','Carella')");
            stmt.executeUpdate("INSERT INTO car (`idcar`,`make`,`person_idperson`) VALUES (1,'Toyota',1)");
            stmt.executeUpdate("INSERT INTO car (`idcar`,`make`,`person_idperson`) VALUES (2,'Ford',null)");

        } finally {
            stmt.close();
        }
    }

    public static Connection getConnection() throws SQLException {
        Connection conn = null;
        Properties connectionProps = new Properties();
        connectionProps.put("user", USERNAME);
        connectionProps.put("password", PASSWORD);

        conn = DriverManager.getConnection(""
                + "jdbc:mysql://" + // protocol for mysql
                "" + IP + // ip of database server
                ":3306" + // mysql port number
                "/" + dbNameArg +// we could specify an  existing database here but we dont need to because our app creates a database
                "", connectionProps);
        return conn;
    }
}
