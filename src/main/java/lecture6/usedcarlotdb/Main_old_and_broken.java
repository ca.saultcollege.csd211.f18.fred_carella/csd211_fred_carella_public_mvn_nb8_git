package lecture6.usedcarlotdb;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author fcarella
 */
public class Main_old_and_broken {

    private static String USERNAME = "root";
    private static String IP = "localhost";
    private static String PASSWORD = "itstudies12345";
    private static String dbNameArg = "usedcarlot";
    private static Connection con = null;
    private static String TABLE_NAME = "car";

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            // get connection.  This connection may or may not be connected to a database
            // Depends on if one exists or not
            // If it doesn't exist we get a connection with which we can create a database.
            try {
                con = getConnection();
            } catch (SQLException ex) {
                Logger.getLogger(Main_old_and_broken.class.getName()).log(Level.SEVERE, null, ex);
            }

            // create the database if it doesn't exist
            Statement s = con.createStatement();
            String newDatabaseString = "CREATE DATABASE IF NOT EXISTS " + dbNameArg;
            // String newDatabaseString = "CREATE DATABASE " + dbName;
            s.executeUpdate(newDatabaseString);
            System.out.println("Created database " + dbNameArg);

            // get the connection again.  This time, we're sure its connected to a
            // database.
            try {
                // TODO code application logic here
                con = getConnection();
            } catch (SQLException ex) {
                Logger.getLogger(Main_old_and_broken.class.getName()).log(Level.SEVERE, null, ex);
            }

            populateTable();

            viewTable();

        } catch (SQLException ex) {
            Logger.getLogger(Main_old_and_broken.class.getName()).log(Level.SEVERE, null, ex);
        }


    }

    public static void viewTable() throws SQLException {
        Statement stmt = null;
        String query =
                "SELECT * from car,person where car.person_idperson=person.idperson";
        try {
            stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {
                int id = rs.getInt("idcar");
                String make = rs.getString("make");
                String firstname = rs.getString("firstname");
                System.out.println(id + "Car: " + make + " Owner: " + firstname);
            }
        } finally {
            stmt.close();
        }
    }


    public static void populateTable() throws SQLException {
        Statement stmt = null;
        try {
            stmt = con.createStatement();

            stmt.executeUpdate("INSERT INTO "+TABLE_NAME+" (`idcar`,`make`,`person_idperson`) VALUES (7,'Toyota',1)");
            stmt.executeUpdate("INSERT INTO "+TABLE_NAME+" (`idcar`,`make`,`person_idperson`) VALUES (8,'KIA',1)");

        } finally {
            stmt.close();
        }
    }

    public static Connection getConnection() throws SQLException {
        Connection conn = null;
        Properties connectionProps = new Properties();
        connectionProps.put("user", USERNAME);
        connectionProps.put("password", PASSWORD);

        try{
            conn = DriverManager.getConnection("" +
                    "jdbc:mysql://" + // protocol for mysql
                    "" + IP + // ip of database server
                    ":3306" + // mysql port number
                    "/" + dbNameArg +// we could specify an  existing database here but we dont need to because our app creates a database
                    "", connectionProps);
        }catch(SQLException e){
            if(e.getMessage().contains("Unknown database")){
                conn = DriverManager.getConnection("" +
                        "jdbc:mysql://" + // protocla for mysql
                        "" + IP + // ip of database server
                        ":3306" + // mysql port number
//                        "/" + dbNameArg +// we could specify an  existing database here but we dont need to because our app creates a database
                        "", connectionProps);
            }
        }
        System.out.println("Connected to database");
        return conn;
    }
}
