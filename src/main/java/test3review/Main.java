package test3review;

import java.util.ArrayList;
import java.util.Iterator;

public class Main {

	// declare array
	private static String anArray[]=new String[10];
	// declare/initialize array
	private static String anArray2[]={"A","B"};
	
	// declare array list: I can put anything in here
	private static ArrayList objectList=new ArrayList();
	// declare a list of Strings: Here I can only put Strings
	private static ArrayList<String> listOfStrings=new ArrayList<String>();
	
	public static void main(String[] args) {
		// populate array
		for(int i=0;i<anArray.length;i++){
			anArray[i]="String "+i;
		}
		// iterate thru array
		for(int i=0;i<anArray.length;i++){
			System.out.println(anArray[i]);
		}
		
		// populate list
		// 
		for(int i=0;i<5;i++){
			if(i%2==0)
				objectList.add("String"+i);
			else
				objectList.add(new Integer(i));
		}
		for(int i=0;i<5;i++){
			listOfStrings.add("String"+i);
//			listOfStrings.add(new Integer(i));
		}
		// iterate through list
		// with an index
		for(int i=0;i<listOfStrings.size();i++){
			System.out.println(listOfStrings.get(i));
		}
		// for each
		for(String s:listOfStrings)
			System.out.println(s);
		// with an iterator
		Iterator<String> it = listOfStrings.iterator();
		while(it.hasNext()){
			System.out.println(it.next());
		}

	}

}
